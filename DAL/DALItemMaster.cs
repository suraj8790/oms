﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DALItemMaster
    {
        
        public int id { get; set; }
        public string Manufacture { get; set; }
        public string Brand { get; set; }
        public string ModelName { get; set; }
        public string SerialNos { get; set; }
        public string Warranty { get; set; }
        public string PurchaseDate { get; set; }
        public decimal PurchasePrice { get; set; }
        public int PQty { get; set; }
        public decimal RetailPrice { get; set; }
        public decimal Discount { get; set; }
        public decimal SalePrice { get; set; }
        public bool ShowOnLive { get; set; }
        public string DisplayName { get; set; }
        public bool IsDeleted { get; set; }

        public string Flag { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string ItemDetail { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Colors { get; set; }
        public decimal Stock { get; set; }
        public string Images { get; set; }
        public bool IsTrending { get; set; }
        public string PrimaryFileName { get; set; }
        public string StockPurchaseDate { get; set; }

    }
}
