﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DALCustomerMaster
    {
        public int CustId { get; set; }
        public int CustItems { get; set; }
        public string CustName { get; set; }
        public string BillingAddres1 { get; set; }
        public string BillingAddres2 { get; set; }
        public string LandMark1 { get; set; }
        public string PinCode1 { get; set; }
        public string ShipingAddres1 { get; set; }
        public string ShipingAddres2 { get; set; }
        public string LandMark2 { get; set; }
        public string PinCode2 { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool   IsDeleted {get; set;}
        public string Flag { get; set; }
    }
}
