﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
  public  class DALOrderMaster
    {
        public int OrderId { get; set;}
        public int CustomerId { get; set;}
        public string Flag { get; set; }
        public decimal TotalPrice { get; set;}
        public decimal DiscountAmount { get; set;}
        public decimal NetAmount { get; set; }
        public string UnderStatus { get; set; }    
        public string OrderDate { get; set; }
        public string OrderUpdateDate { get; set; }
        public string AddressType { get; set; }
        public string stDetailXml { get; set; }
        public bool IsDeleted { get; set; }
    }
}
