﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace BAL
{
    public class BALCustomerMaster
    {
        CommonFunctions CommonFunction = new CommonFunctions();

        public DataTable getCustomer(DALCustomerMaster dalItem)
        {
            DataTable DT = new DataTable();
            SqlParameter[] Sqlpara = new SqlParameter[3];
            Sqlpara[0] = new SqlParameter("@Flag", DbType.String);
            Sqlpara[0].Value = dalItem.Flag;
            Sqlpara[1] = new SqlParameter("@CustId", DbType.String);
            Sqlpara[1].Value = dalItem.CustId;
            Sqlpara[2] = new SqlParameter("@CustName", DbType.String);
            Sqlpara[2].Value = dalItem.CustName;
            DT = CommonFunction.SelectRecords("Select_CustomerMaster", Sqlpara);
            return DT;
        }

        public DataTable getCustomerLogin(DALCustomerMaster dalItem)
        {
            DataTable DT = new DataTable();
            SqlParameter[] Sqlpara = new SqlParameter[3];
            Sqlpara[0] = new SqlParameter("@Flag", DbType.String);
            Sqlpara[0].Value = dalItem.Flag;
            Sqlpara[1] = new SqlParameter("@MobileNo", DbType.String);
            Sqlpara[1].Value = dalItem.MobileNo;
            Sqlpara[2] = new SqlParameter("@Password", DbType.String);
            Sqlpara[2].Value = dalItem.Password;
            DT = CommonFunction.SelectRecords("CustomerLoginNew", Sqlpara);
            return DT;
        }

        public Int32 InsertCustomer(DALCustomerMaster dalItem)
        {
            DataTable DT = new DataTable();
            SqlParameter[] Sqlpara = new SqlParameter[14];
            Sqlpara[0] = new SqlParameter("@CustId", DbType.String);
            Sqlpara[0].Value = dalItem.CustId;
            Sqlpara[1] = new SqlParameter("@CustName", DbType.String);
            Sqlpara[1].Value = dalItem.CustName;
            Sqlpara[2] = new SqlParameter("@BillingAddres1", DbType.String);
            Sqlpara[2].Value = dalItem.BillingAddres1;
            Sqlpara[3] = new SqlParameter("@BillingAddres2", DbType.String);
            Sqlpara[3].Value = dalItem.BillingAddres2;
            Sqlpara[4] = new SqlParameter("@LandMark1", DbType.String);
            Sqlpara[4].Value = dalItem.LandMark1;
            Sqlpara[5] = new SqlParameter("@PinCode1", DbType.String);
            Sqlpara[5].Value = dalItem.PinCode1;
            Sqlpara[6] = new SqlParameter("@ShipingAddres1", DbType.String);
            Sqlpara[6].Value = dalItem.ShipingAddres1;
            Sqlpara[7] = new SqlParameter("@ShipingAddres2", DbType.String);
            Sqlpara[7].Value = dalItem.ShipingAddres2;
            Sqlpara[8] = new SqlParameter("@LandMark2", DbType.String);
            Sqlpara[8].Value = dalItem.LandMark2;
            Sqlpara[9] = new SqlParameter("@PinCode2", DbType.String);
            Sqlpara[9].Value = dalItem.PinCode2;
            Sqlpara[10] = new SqlParameter("@MobileNo", DbType.String);
            Sqlpara[10].Value = dalItem.MobileNo;
            Sqlpara[11] = new SqlParameter("@Email", DbType.String);
            Sqlpara[11].Value = dalItem.Email;          
            Sqlpara[12] = new SqlParameter("@CustItems", DbType.String);
            Sqlpara[12].Value = dalItem.CustItems;
            Sqlpara[13] = new SqlParameter("@Password", DbType.String);
            Sqlpara[13].Value = dalItem.Password;

            return CommonFunction.InsertRecord("InsertUpdateCustomerMaster", Sqlpara);
        }

        public Int32 UpdateCustomer(DALCustomerMaster dalItem)
        {
            DataTable DT = new DataTable();
            SqlParameter[] Sqlpara = new SqlParameter[14];
            Sqlpara[0] = new SqlParameter("@CustId", DbType.String);
            Sqlpara[0].Value = dalItem.CustId;
            Sqlpara[1] = new SqlParameter("@CustName", DbType.String);
            Sqlpara[1].Value = dalItem.CustName;
            Sqlpara[2] = new SqlParameter("@BillingAddres1", DbType.String);
            Sqlpara[2].Value = dalItem.BillingAddres1;
            Sqlpara[3] = new SqlParameter("@BillingAddres2", DbType.String);
            Sqlpara[3].Value = dalItem.BillingAddres2;
            Sqlpara[4] = new SqlParameter("@LandMark1", DbType.String);
            Sqlpara[4].Value = dalItem.LandMark1;
            Sqlpara[5] = new SqlParameter("@Pincode1", DbType.String);
            Sqlpara[5].Value = dalItem.PinCode1;
            Sqlpara[6] = new SqlParameter("@ShipingAddres1", DbType.String);
            Sqlpara[6].Value = dalItem.ShipingAddres1;
            Sqlpara[7] = new SqlParameter("@ShipingAddres2", DbType.String);
            Sqlpara[7].Value = dalItem.ShipingAddres2;
            Sqlpara[8] = new SqlParameter("@LandMark2", DbType.String);
            Sqlpara[8].Value = dalItem.LandMark2;
            Sqlpara[9] = new SqlParameter("@PinCode2", DbType.String);
            Sqlpara[9].Value = dalItem.PinCode2;
            Sqlpara[10] = new SqlParameter("@MobileNo", DbType.String);
            Sqlpara[10].Value = dalItem.MobileNo;
            Sqlpara[11] = new SqlParameter("@Email", DbType.String);
            Sqlpara[11].Value = dalItem.Email;
            Sqlpara[12] = new SqlParameter("@CustItems", DbType.String);
            Sqlpara[12].Value = dalItem.CustItems;
            Sqlpara[13] = new SqlParameter("@Password", DbType.String);
            Sqlpara[13].Value = dalItem.Password;

            return CommonFunction.InsertRecord("InsertUpdateCustomerMaster", Sqlpara);
        }
    }

   
}
