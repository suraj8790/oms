﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace BAL
{
    public class BALItemMaster
    {
        CommonFunctions commonFunction = new CommonFunctions();

        public DataTable getItems(DALItemMaster dalItem)
        {
            DataTable Dt = new DataTable();
            SqlParameter[] SqlPara = new SqlParameter[6];
            SqlPara[0] = new SqlParameter("@Flag", DbType.String);
            SqlPara[0].Value = dalItem.Flag;
            SqlPara[1] = new SqlParameter("@id", DbType.String);
            SqlPara[1].Value = dalItem.id;
            SqlPara[2] = new SqlParameter("@ItemName", DbType.String);
            SqlPara[2].Value = dalItem.DisplayName;
            SqlPara[3] = new SqlParameter("@FromDate", DbType.String);
            SqlPara[3].Value = dalItem.FromDate;
            SqlPara[4] = new SqlParameter("@ToDate", DbType.String);
            SqlPara[4].Value = dalItem.ToDate;
            SqlPara[5] = new SqlParameter("@CategoryId", DbType.String);
            SqlPara[5].Value = dalItem.CategoryId;
            Dt = commonFunction.SelectRecords("Select_ItemMaster", SqlPara);
            return Dt;
        }

        public DataTable getItemsForOnline(DALItemMaster dalItem)
        {
            DataTable Dt = new DataTable();
            SqlParameter[] SqlPara = new SqlParameter[6];
            SqlPara[0] = new SqlParameter("@Flag", DbType.String);
            SqlPara[0].Value = dalItem.Flag;
            SqlPara[1] = new SqlParameter("@id", DbType.String);
            SqlPara[1].Value = dalItem.id;
            SqlPara[2] = new SqlParameter("@ItemName", DbType.String);
            SqlPara[2].Value = dalItem.DisplayName;
            SqlPara[3] = new SqlParameter("@FromDate", DbType.String);
            SqlPara[3].Value = dalItem.FromDate;
            SqlPara[4] = new SqlParameter("@ToDate", DbType.String);
            SqlPara[4].Value = dalItem.ToDate;
            SqlPara[5] = new SqlParameter("@CatgoryId", DbType.String);
            SqlPara[5].Value = dalItem.CategoryId;
            Dt = commonFunction.SelectRecords("Onlinedataprovider", SqlPara);
            return Dt;
        }

        public DataTable getCategories()
        {
            DataTable Dt = new DataTable();
            SqlParameter[] SqlPara = new SqlParameter[1];
            SqlPara[0] = new SqlParameter("@catName", DbType.String);
            SqlPara[0].Value = "";

            Dt = commonFunction.SelectRecords("getCategoryList", SqlPara);
            return Dt;
        }

        public Int32 InsertCategory(DALItemMaster dalItem)
        {
            DataTable Dt = new DataTable();
            SqlParameter[] SqlPara = new SqlParameter[2];
            SqlPara[0] = new SqlParameter("@CatId", DbType.String);
            SqlPara[0].Value = "";
            SqlPara[1] = new SqlParameter("@CatName", DbType.String);
            SqlPara[1].Value = dalItem.CategoryName;

            return commonFunction.InsertRecord("InsertUpdateCategory", SqlPara);

        }


        public Int32 InsertItem(DALItemMaster dalItem)
        {
            DataTable Dt = new DataTable();
            SqlParameter[] SqlPara = new SqlParameter[20];
            SqlPara[0] = new SqlParameter("@id", DbType.String);
            SqlPara[0].Value = dalItem.id;
            SqlPara[1] = new SqlParameter("@Manufacture", DbType.String);
            SqlPara[1].Value = dalItem.Manufacture;
            SqlPara[2] = new SqlParameter("@Brand", DbType.String);
            SqlPara[2].Value = dalItem.Brand;
            SqlPara[3] = new SqlParameter("@ModelName", DbType.String);
            SqlPara[3].Value = dalItem.ModelName;
            SqlPara[4] = new SqlParameter("@SerialNos", DbType.String);
            SqlPara[4].Value = dalItem.SerialNos;
            SqlPara[5] = new SqlParameter("@Warranty", DbType.String);
            SqlPara[5].Value = dalItem.Warranty;
            SqlPara[6] = new SqlParameter("@PurchaseDate", DbType.String);
            SqlPara[6].Value = dalItem.PurchaseDate;
            SqlPara[7] = new SqlParameter("@PQty", DbType.String);
            SqlPara[7].Value = dalItem.PQty;
            SqlPara[8] = new SqlParameter("@RetailPrice", DbType.String);
            SqlPara[8].Value = dalItem.RetailPrice;
            SqlPara[9] = new SqlParameter("@Discount", DbType.String);
            SqlPara[9].Value = dalItem.Discount;
            SqlPara[10] = new SqlParameter("@SalePrice", DbType.String);
            SqlPara[10].Value = dalItem.SalePrice;
            SqlPara[11] = new SqlParameter("@DisplayName", DbType.String);
            SqlPara[11].Value = dalItem.DisplayName;
            SqlPara[12] = new SqlParameter("@ShowOnLive", DbType.String);
            SqlPara[12].Value = dalItem.ShowOnLive;
            SqlPara[13] = new SqlParameter("@CatgoryId", DbType.String);
            SqlPara[13].Value = dalItem.CategoryId;
            SqlPara[14] = new SqlParameter("@Colors", DbType.String);
            SqlPara[14].Value = dalItem.Colors;
            SqlPara[15] = new SqlParameter("@ItemDetails", DbType.String);
            SqlPara[15].Value = dalItem.ItemDetail;
            SqlPara[16] = new SqlParameter("@PurchasePrice", DbType.String);
            SqlPara[16].Value = dalItem.PurchasePrice;
            SqlPara[17] = new SqlParameter("@Images", DbType.String);
            SqlPara[17].Value = dalItem.Images;
            SqlPara[18] = new SqlParameter("@IsTrending", DbType.String);
            SqlPara[18].Value = dalItem.IsTrending;
            SqlPara[19] = new SqlParameter("@PrimaryFileName", DbType.String);
            SqlPara[19].Value = dalItem.PrimaryFileName;


            return commonFunction.InsertRecord("InsertUpdate_ItemMaster", SqlPara);

        }

        public Int32 UpdateItem(DALItemMaster dalItem)
        {
            DataTable Dt = new DataTable();
            SqlParameter[] SqlPara = new SqlParameter[20];
            SqlPara[0] = new SqlParameter("@id", DbType.String);
            SqlPara[0].Value = dalItem.id;
            SqlPara[1] = new SqlParameter("@Manufacture", DbType.String);
            SqlPara[1].Value = dalItem.Manufacture;
            SqlPara[2] = new SqlParameter("@Brand", DbType.String);
            SqlPara[2].Value = dalItem.Brand;
            SqlPara[3] = new SqlParameter("@ModelName", DbType.String);
            SqlPara[3].Value = dalItem.ModelName;
            SqlPara[4] = new SqlParameter("@SerialNos", DbType.String);
            SqlPara[4].Value = dalItem.SerialNos;
            SqlPara[5] = new SqlParameter("@Warranty", DbType.String);
            SqlPara[5].Value = dalItem.Warranty;
            SqlPara[6] = new SqlParameter("@PurchaseDate", DbType.String);
            SqlPara[6].Value = dalItem.PurchaseDate;
            SqlPara[7] = new SqlParameter("@PQty", DbType.String);
            SqlPara[7].Value = dalItem.PQty;
            SqlPara[8] = new SqlParameter("@RetailPrice", DbType.String);
            SqlPara[8].Value = dalItem.RetailPrice;
            SqlPara[9] = new SqlParameter("@Discount", DbType.String);
            SqlPara[9].Value = dalItem.Discount;
            SqlPara[10] = new SqlParameter("@SalePrice", DbType.String);
            SqlPara[10].Value = dalItem.SalePrice;
            SqlPara[11] = new SqlParameter("@DisplayName", DbType.String);
            SqlPara[11].Value = dalItem.DisplayName;
            SqlPara[12] = new SqlParameter("@ShowOnLive", DbType.String);
            SqlPara[12].Value = dalItem.ShowOnLive;
            SqlPara[13] = new SqlParameter("@CatgoryId", DbType.String);
            SqlPara[13].Value = dalItem.CategoryId;
            SqlPara[14] = new SqlParameter("@Colors", DbType.String);
            SqlPara[14].Value = dalItem.Colors;
            SqlPara[15] = new SqlParameter("@ItemDetails", DbType.String);
            SqlPara[15].Value = dalItem.ItemDetail;
            SqlPara[16] = new SqlParameter("@PurchasePrice", DbType.String);
            SqlPara[16].Value = dalItem.PurchasePrice;
            SqlPara[17] = new SqlParameter("@Images", DbType.String);
            SqlPara[17].Value = dalItem.Images;
            SqlPara[18] = new SqlParameter("@IsTrending", DbType.String);
            SqlPara[18].Value = dalItem.IsTrending;
            SqlPara[19] = new SqlParameter("@PrimaryFileName", DbType.String);
            SqlPara[19].Value = dalItem.PrimaryFileName;


            return commonFunction.InsertRecord("InsertUpdate_ItemMaster", SqlPara);

        }

        public Int32 DeletetItem(DALItemMaster dalItem)
        {
            DataTable Dt = new DataTable();
            SqlParameter[] SqlPara = new SqlParameter[1];
            SqlPara[0] = new SqlParameter("@id", DbType.String);
            SqlPara[0].Value = dalItem.id;
            //SqlPara[1] = new SqlParameter("@DeletedById", DbType.String);
            //SqlPara[1].Value = dalItem.DeletedById;
            //SqlPara[2] = new SqlParameter("@DeletedDate", DbType.String);
            //SqlPara[2].Value = dalItem.DeletedDate;

            return commonFunction.InsertRecord("Delete_ItemMaster", SqlPara);

        }

        public Int32 AddUpdateStock(DALItemMaster dalItem)
        {
            DataTable Dt = new DataTable();
            SqlParameter[] SqlPara = new SqlParameter[4];
            SqlPara[0] = new SqlParameter("@id", DbType.String);
            SqlPara[0].Value = dalItem.id;
            SqlPara[1] = new SqlParameter("@Flag", DbType.String);
            SqlPara[1].Value = dalItem.Flag;
            SqlPara[2] = new SqlParameter("@Stock", DbType.String);
            SqlPara[2].Value = dalItem.Stock;
            SqlPara[3] = new SqlParameter("@Podate", DbType.String);
            SqlPara[3].Value = dalItem.StockPurchaseDate;

            return commonFunction.InsertRecord("AddUpdateStock", SqlPara);

        }
    }
}
