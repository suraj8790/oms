﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;


namespace BAL
{
 public class BALOrderMaster
    {
            CommonFunctions CommonFunction = new CommonFunctions();

            public DataTable getOrder(DALOrderMaster dalItem)
            {
                DataTable DT = new DataTable();
                SqlParameter[] Sqlpara = new SqlParameter[3];
                Sqlpara[0] = new SqlParameter("@OrderId", DbType.String);
                Sqlpara[0].Value = dalItem.OrderId;
                Sqlpara[1] = new SqlParameter("@CustomerId", DbType.String);
                Sqlpara[1].Value = dalItem.CustomerId;
                Sqlpara[2] = new SqlParameter("@Flag", DbType.String);
                Sqlpara[2].Value = dalItem.Flag;


                DT = CommonFunction.SelectRecords("Select_OrderMaster", Sqlpara);
                return DT;
            }

            public Int32 InsertOrder(DALOrderMaster dalItem)
            {
                DataTable DT = new DataTable();
                SqlParameter[] Sqlpara = new SqlParameter[10];
                Sqlpara[0] = new SqlParameter("@OrderId", DbType.String);
                Sqlpara[0].Value = dalItem.OrderId;
                Sqlpara[1] = new SqlParameter("@CustomerId", DbType.String);
                Sqlpara[1].Value = dalItem.CustomerId;
                Sqlpara[2] = new SqlParameter("@TotalPrice", DbType.String);
                Sqlpara[2].Value = dalItem.TotalPrice;
                Sqlpara[3] = new SqlParameter("@DiscountAmount", DbType.String);
                Sqlpara[3].Value = dalItem.DiscountAmount;
                Sqlpara[4] = new SqlParameter("@NetAmount", DbType.String);
                Sqlpara[4].Value = dalItem.NetAmount;
                Sqlpara[5] = new SqlParameter("@UnderStatus", DbType.String);
                Sqlpara[5].Value = dalItem.UnderStatus;
                Sqlpara[6] = new SqlParameter("@OrderDate", DbType.String);
                Sqlpara[6].Value = dalItem.OrderDate;
                Sqlpara[7] = new SqlParameter("@OrderUpdateDate", DbType.String);
                Sqlpara[7].Value = dalItem.OrderUpdateDate;
                Sqlpara[8] = new SqlParameter("@AddressType", DbType.String);
                Sqlpara[8].Value = dalItem.AddressType;
                Sqlpara[9] = new SqlParameter("@stDetailXml", DbType.String);
                Sqlpara[9].Value = dalItem.stDetailXml;

            return CommonFunction.InsertRecord("Tran_Insert_Order", Sqlpara);
            }

            public Int32 UpdateOrder(DALOrderMaster dalItem)
            {
                DataTable DT = new DataTable();
                SqlParameter[] Sqlpara = new SqlParameter[9];
                Sqlpara[0] = new SqlParameter("@OrdrId", DbType.String);
                Sqlpara[0].Value = dalItem.OrderId;
                Sqlpara[1] = new SqlParameter("@CustomerId", DbType.String);
                Sqlpara[1].Value = dalItem.CustomerId;
                Sqlpara[2] = new SqlParameter("@TotalPrice", DbType.String);
                Sqlpara[2].Value = dalItem.TotalPrice;
                Sqlpara[3] = new SqlParameter("@DiscountAmount", DbType.String);
                Sqlpara[3].Value = dalItem.DiscountAmount;
                Sqlpara[4] = new SqlParameter("@NetAmount", DbType.String);
                Sqlpara[4].Value = dalItem.NetAmount;
                Sqlpara[5] = new SqlParameter("@UnderStatus", DbType.String);
                Sqlpara[5].Value = dalItem.UnderStatus;
                Sqlpara[6] = new SqlParameter("@OrderDate", DbType.String);
                Sqlpara[6].Value = dalItem.OrderDate;
                Sqlpara[7] = new SqlParameter("@OrderUpdateDate", DbType.String);
                Sqlpara[7].Value = dalItem.OrderUpdateDate;
                Sqlpara[8] = new SqlParameter("@stDetailXml", DbType.String);
                Sqlpara[8].Value = dalItem.stDetailXml;

            return CommonFunction.InsertRecord("InsertUpdateOrder_Master", Sqlpara);
            }
        }
    
}
