$(document).ready(function () {
  $("#navbarSupportedContent ul.main-nav li").click(function (event) {
    event.preventDefault();
    if (!$(this).hasClass("active")) {
      $(this).siblings().removeClass("active");
      $(this).addClass("active");
      var activatetab=$(this).attr('data-activate');
      $('section.admin').slideUp();
      $("#"+activatetab).slideDown();
    }
  });

  //$(function () {
  //  var availableTags = [
  //    "Mobile Charger",
  //    "USB Data Cable",
  //    "OTG Cable",
  //    "Cell Phone Data Cable",
  //    "iPhone Data Cable",
  //    "Mobile Sanitizer",
  //    "Multifunction Mobile Charger Cable",
  //    "Sim Card Reader",
  //    "Mobile Ring",
  //    "Mobile Phone Speaker",
  //    "Bluetooth Accessories",
  //    "Plastic Screen guard",
  //    "Mobile Phone Connector",
  //    "Mobile Phone Cleaner",
  //    "Calling Card",
  //  ];
  //  $("#ProductCategory").autocomplete({
  //    source: availableTags,
  //    open: function (event, ui) {
  //      search_term = event.target.value;
  //      var d = $(".ui-autocomplete").append(
  //        "<li class='ui-menu-item addcategory' data-toggle='modal' data-target='#AddCategory'>Add New Category </li>"
  //      );
  //    },
  //  });
  //});
});
