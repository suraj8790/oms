﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BAL;
using System.Data;

namespace OnlineShopping.UI
{
    public partial class ItemMaster : System.Web.UI.Page
    {
        public static string UserName = "";
        public static string UserId = "";

        DALItemMaster dalItem = new DALItemMaster();
        BALItemMaster balItem = new BALItemMaster();
        CommonFunctions commonFunction = new CommonFunctions();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillMasterDropDownList();
                ClearText();
                FillGrid();
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            Master.SendbtnSaveCat.Click += new EventHandler(btnMasterPageSaveCat_Click);
            Master.SendbtnSaveStock.Click += new EventHandler(rbdMasterPageAddStock_Click);
            Master.SendrdbAddStock.CheckedChanged += new EventHandler(SendrdbAddStock_checkChanged);
            Master.SendrdbRemoveStock.CheckedChanged += new EventHandler(SendrdbRemoveStock_checkChanged);
        }

      

        private void FillGrid()
        {
            try
            {
                grdSearch.DataSource = null;
                grdSearch.DataBind();
                dalItem.Flag = "FillGrid";
                DataTable dt = new DataTable();
                dt = balItem.getItems(dalItem);
                if (dt.Rows.Count > 0)
                {
                    grdSearch.DataSource = dt;
                    grdSearch.DataBind();
                    if (grdSearch.Rows.Count > 0)
                    {
                        grdSearch.UseAccessibleHeader = true;
                        grdSearch.HeaderRow.TableSection = TableRowSection.TableHeader;
                        grdSearch.FooterRow.TableSection = TableRowSection.TableFooter;
                    }
                }
            }
            catch (Exception)
            {

            }
        }
        private void FillMasterDropDownList()
        {
            try
            {
                DataTable dt = new DataTable();
                dt = balItem.getCategories();
                DataRow dr = dt.NewRow();
                dr[0] = "0";
                dr[1] = "-- Select Category --";

                dt.Rows.InsertAt(dr, 0);

                if (dt.Rows.Count > 0)
                {
                    drpCategory.DataSource = dt;
                    drpCategory.DataTextField = "CatName";
                    drpCategory.DataValueField = "CatId";
                    drpCategory.DataBind();
                }


            }
            catch (Exception)
            {
            }
        }

        public void ClearText()
        {
            txtItemDetail.Text = "";
            txtManufactureName.Text = "";
            txtBrandName.Text = "";
            txtModelName.Text = "";
            txtSerialNo.Text = "";
            txtWarranty.Text = "";
            txtPurchaseDate.Text = "";
            txtDisplayName.Text = "";
            txtPurchasePrice.Text = "0.00";
            txtQuantity.Text = " ";
            txtPrice.Text = "0.00";
            txtDiscount.Text = "0.00";
            txtSalePrice.Text = "0.00";
            txtColors.Text = "";
            chkShowOnLive.Checked = true;
            drpCategory.ClearSelection();
            hdnID.Value = "";
            hdnMode.Value = "I";
            lblFileNames.Text = "";
            divUploadedImages.InnerHtml = "";
            div1SingleUploadImages.InnerHtml = "";
            chkIsTrending.Checked = false;
          //  PrimaryFileName.Text = "";

        }

        protected void drpCategory_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 _strisexist;


                // CheckIsNullAndConvertToDecimal instead CheckIsNull

                dalItem.ItemDetail = isNullorEmptyString(txtItemDetail.Text) ? "" : txtItemDetail.Text.ToString().Trim();
                dalItem.Manufacture = isNullorEmptyString(txtManufactureName.Text) ? "" : txtManufactureName.Text.ToString().Trim();
                dalItem.Brand = isNullorEmptyString(txtBrandName.Text) ? "" : txtBrandName.Text.ToString().Trim();
                dalItem.ModelName = isNullorEmptyString(txtModelName.Text) ? "" : txtModelName.Text.ToString().Trim();
                dalItem.SerialNos = isNullorEmptyString(txtSerialNo.Text) ? "" : txtSerialNo.Text.ToString().Trim();
                dalItem.Warranty = isNullorEmptyString(txtWarranty.Text) ? "" : txtWarranty.Text.ToString().Trim();
                dalItem.PurchaseDate = isNullorEmptyString(txtPurchaseDate.Text) ? "" : txtPurchaseDate.Text.ToString().Trim();
                dalItem.DisplayName = isNullorEmptyString(txtDisplayName.Text) ? "" : txtDisplayName.Text.ToString().Trim();
                dalItem.PurchasePrice = commonFunction.CheckIsNullAndConvertToDecimal(isNullorEmptyString(txtPurchasePrice.Text) ? "" : txtPurchasePrice.Text.ToString().Trim());
                dalItem.PQty = Convert.ToInt32(isNullorEmptyString(txtQuantity.Text) ? "0" : txtQuantity.Text.ToString().Trim());
                dalItem.RetailPrice = commonFunction.CheckIsNullAndConvertToDecimal(isNullorEmptyString(txtPrice.Text) ? "" : txtPrice.Text.ToString().Trim());
                dalItem.Discount = commonFunction.CheckIsNullAndConvertToDecimal(isNullorEmptyString(txtDiscount.Text) ? "" : txtDiscount.Text.ToString().Trim());
                dalItem.SalePrice = commonFunction.CheckIsNullAndConvertToDecimal(isNullorEmptyString(txtSalePrice.Text) ? "" : txtSalePrice.Text.ToString().Trim());
                dalItem.ShowOnLive = chkShowOnLive.Checked;
                dalItem.CategoryId = Convert.ToInt32(drpCategory.SelectedValue);
                dalItem.Colors = isNullorEmptyString(txtColors.Text) ? "" : txtColors.Text.ToString().Trim();
                dalItem.Images = lblFileNames.Text;
                dalItem.IsTrending = chkIsTrending.Checked;
                dalItem.PrimaryFileName = SinglelblFileNames.Text;
                if (hdnMode.Value == "I")
                {
                    _strisexist = commonFunction.IsExist("ItemMaster", "IsDeleted=0 and DisplayName='" + txtDisplayName.Text + "' and CatgoryId=" + drpCategory.SelectedValue + " ");
                    if (_strisexist == 0)
                    {
                        //_dalitem.CreatedByID = Convert.ToInt32(Session["UserId"].ToString());
                        //_dalitem.CreatedDate = System.DateTime.Today.Date.ToShortDateString();
                        dalItem.Flag = "Insert";
                        int count = balItem.InsertItem(dalItem);
                        if (count > 0)
                        {

                            ClearText();
                            FillGrid();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "openSuccessModal('Success!','Item Added successfully !');", true);
                            //DivData.Visible = false;
                            //DivSearch.Visible = true;
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "openErrorModal('Error!','Item name already exist !');", true);
                    }
                }
                else if (hdnMode.Value == "U")
                {
                    dalItem.id = Convert.ToInt32(hdnID.Value);
                    //_dalitem.UpdatedByID = Convert.ToInt32(Session["UserId"].ToString());
                    // _dalitem.UpdatedDate = System.DateTime.Today.Date.ToShortDateString();
                    dalItem.Flag = "Update";
                    int count = balItem.UpdateItem(dalItem);
                    if (count > 0)
                    {

                        ClearText();
                        FillGrid();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "openSuccessModal('Success!','Item updated successfully !');", true);
                        //DivData.Visible = false;
                        //DivSearch.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "openErrorModal('Error!','Error Occured');", true);
            }
        }

        protected void btnCancel_Click(object Sender, EventArgs e)
        {
            ClearText();
        }

        public bool isNullorEmptyString(string str)
        {
            bool isNullorEmpty = false;
            try
            {
                if (str == null || str == "")
                {
                    isNullorEmpty = true;
                }
            }
            catch (Exception)
            {


            }

            return isNullorEmpty;
        }

        protected void btnMasterPageSaveCat_Click(object sender, EventArgs e)
        {
            try
            {
                dalItem.CategoryName = Master.SendCatNameText.Text;

                int count = balItem.InsertCategory(dalItem);
                if (count > 0)
                {
                    FillMasterDropDownList();
                    DataTable dt = new DataTable();
                    dt = balItem.getCategories();
                    drpCategory.SelectedIndex = dt.Rows.Count;
                    Master.SendCatNameText.Text = "";

                }

            }
            catch (Exception)
            {

                throw;
            }

        }

        protected void rbdMasterPageAddStock_Click(object sender, EventArgs e)
        {
            try
            {
                Master.setActiveTab("inventory");
                dalItem.Stock = commonFunction.CheckIsNullAndConvertToDecimal(Master.SendQuantityText.Text);
                dalItem.id = Convert.ToInt32(hdnID.Value);
                dalItem.StockPurchaseDate = Master.SendStockPurchaseDate.Text;
                if (Master.SendrdbAddStock.Checked == true)
                {
                    dalItem.Flag = "Add";
                }
                else
                {
                    dalItem.Flag = "Update";
                }
                int count = balItem.AddUpdateStock(dalItem);
                if (count > 0)
                {
                    FillGrid();
                    Master.SendQuantityText.Text = "";
                    Master.SendStockPurchaseDate.Text = "";
                }
                Master.setActiveTab("inventory");
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void SendrdbAddStock_checkChanged(object sender, EventArgs e)
        {
            if(Master.SendrdbAddStock.Checked == true)
            {
                Master.SendStockPurchaseDate.Visible = true;
            }
            else
            {
                Master.SendStockPurchaseDate.Visible = false;
            }
        }

        protected void SendrdbRemoveStock_checkChanged(object sender,EventArgs e)
        {
            if (Master.SendrdbRemoveStock.Checked == true)
            {
                Master.SendStockPurchaseDate.Visible = false;
            }
            else
            {
                Master.SendStockPurchaseDate.Visible = true;
            }
        }



        protected void grdSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdSearch.PageIndex = e.NewPageIndex;
            FillGrid();
        }

        protected void grdSearch_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void grdSearch_RowEditing(object sender, GridViewEditEventArgs e)
        {
            e.NewEditIndex = -1;
        }

        protected void grdSearch_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Edit"))
                {
                    string id = e.CommandArgument.ToString();
                    dalItem.id = Convert.ToInt32(id);
                    hdnID.Value = id;
                    dalItem.Flag = "SelectOne";
                    DataTable dt = new DataTable();
                    dt = balItem.getItems(dalItem);
                    if (dt.Rows.Count > 0)
                    {
                        hdnID.Value = id.ToString();
                        hdnMode.Value = "U";
                        txtItemDetail.Text = dt.Rows[0]["ItemDetails"].ToString();
                        txtManufactureName.Text = dt.Rows[0]["Manufacture"].ToString();
                        txtBrandName.Text = dt.Rows[0]["Brand"].ToString();
                        txtModelName.Text = dt.Rows[0]["ModelName"].ToString();
                        txtSerialNo.Text = dt.Rows[0]["SerialNos"].ToString();
                        txtWarranty.Text = dt.Rows[0]["Warranty"].ToString();
                        txtPurchaseDate.Text = dt.Rows[0]["PurchaseDate"].ToString();
                        txtDisplayName.Text = dt.Rows[0]["DisplayName"].ToString();
                        txtPurchasePrice.Text = dt.Rows[0]["PurchasePrice"].ToString();
                        txtPrice.Text = dt.Rows[0]["RetailPrice"].ToString();
                        txtDiscount.Text = dt.Rows[0]["Discount"].ToString();
                        txtSalePrice.Text = dt.Rows[0]["SalePrice"].ToString();
                        txtColors.Text = dt.Rows[0]["Colors"].ToString();
                        txtQuantity.Text = dt.Rows[0]["PQty"].ToString();
                        drpCategory.SelectedValue = dt.Rows[0]["CatgoryId"].ToString();
                        chkShowOnLive.Checked = Convert.ToBoolean(dt.Rows[0]["ShowOnLive"].ToString());
                        chkIsTrending.Checked = Convert.ToBoolean(dt.Rows[0]["IsTrending"].ToString());
                        string FileNames = dt.Rows[0]["Images"].ToString();
                        string[] FileList = FileNames.Split(',');
                        string imgInnerHtml = "";
                        for (int i = 0; i < FileList.Count(); i++)
                        {
                            imgInnerHtml = imgInnerHtml + "<img src='Images/" + FileList[i] + "' />";
                        }
                        divUploadedImages.InnerHtml = imgInnerHtml;
                        Master.setActiveTab("addNewProduct");

                        string FileNames1 = dt.Rows[0]["PrimaryFileName"].ToString();
                        string[] FileList1 = FileNames1.Split(',');
                        string imgInnerHtml1 = "";
                        for (int i = 0; i < FileList1.Count(); i++)
                        {
                            imgInnerHtml1 = imgInnerHtml1 + "<img src='Images/" + FileList1[i] + "'/>";
                        }
                        div1SingleUploadImages.InnerHtml = imgInnerHtml1;
                        Master.setActiveTab("addNewProduct");
                        //AddNewProduct.Visible = true;
                        //Inventory.Visible = false;
                    }
                }
                else if (e.CommandName.Equals("Delete"))
                {
                    string id = e.CommandArgument.ToString();
                    hdnID.Value = id;
                    dalItem.id = Convert.ToInt32(id);
                    dalItem.Flag = "Delete";
                    int count = balItem.DeletetItem(dalItem);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "openSuccessModal('Success!','Item removed successfully !');", true);
                    ClearText();
                    FillGrid();
                    //AddNewProduct.Visible = false;
                    //Inventory.Visible = true;
                    //Master.setActiveTab("inventory");
                }
                else if (e.CommandName.Equals("AddStock"))
                {
                    string id = e.CommandArgument.ToString();
                    hdnID.Value = id;
                    //AddNewProduct.Visible = false;
                    //Inventory.Visible = true;
                    
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        protected void uploadedFile_Click(object sender, EventArgs e)
        {
            if (UploadImages.HasFiles)
            {
                string imgString = "";
                string FileNames = "";
                foreach (HttpPostedFile uploadedFile in UploadImages.PostedFiles)
                {
                    uploadedFile.SaveAs(System.IO.Path.Combine(Server.MapPath("~/Images/"), uploadedFile.FileName));
                    imgString += "<img src='Images/" + String.Format("{0}<br />", uploadedFile.FileName + "' />");
                    if (FileNames != "")
                    {
                        FileNames = FileNames + "," + uploadedFile.FileName;
                    }
                    else
                    {
                        FileNames = FileNames + uploadedFile.FileName;
                    }
                }
                divUploadedImages.InnerHtml = imgString;
                lblFileNames.Text = FileNames;
                dalItem.Images = FileNames;
            }
        }

        protected void txtManufactureName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtDisplayName.Text = txtManufactureName.Text + " " + txtBrandName.Text + " " + txtModelName.Text;
            }
            catch (Exception ex)
            {

            }
        }

        protected void txtBrandName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtDisplayName.Text = txtManufactureName.Text + " " + txtBrandName.Text + " " + txtModelName.Text;
            }
            catch (Exception ex)
            {

            }
        }

        protected void txtModelName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtDisplayName.Text = txtManufactureName.Text + " " + txtBrandName.Text + " " + txtModelName.Text;
            }
            catch (Exception ex)
            {

            }
        }



        private double calculateDiscount(string from)
        {
            try

            {
                double RetailPrice = 0.00F, Discount = 0.00F, SalePrice = 0.00F, discountamount = 0.00F;
                RetailPrice = txtPrice.Text != "" ? Convert.ToDouble(txtPrice.Text) : 0.00;
                Discount = txtDiscount.Text != "" ? Convert.ToDouble(txtDiscount.Text) : 0.00;
                SalePrice = txtSalePrice.Text != "" ? Convert.ToDouble(txtSalePrice.Text) : 0.00;

                //discountamount = RetailPrice * (Discount / 100);

                //Discount = (discountamount / RetailPrice) * 100;

                //RetailPrice = (discountamount * 100) / Discount;

                //SalePrice = RetailPrice - discountamount;

                //txtSalePrice.Text = SalePrice.ToString();
                //txtDiscount.Text = Discount.ToString();
                if (from == "retail")
                {
                    discountamount = RetailPrice * (Discount / 100);
                    SalePrice = RetailPrice - discountamount;
                    if (SalePrice.ToString() != "NaN")
                        txtSalePrice.Text = SalePrice.ToString();
                }
                if (from == "disc")
                {
                    discountamount = RetailPrice * (Discount / 100);
                    SalePrice = RetailPrice - discountamount;
                    if (SalePrice.ToString() != "NaN")
                        txtSalePrice.Text = SalePrice.ToString();
                }
                if (from == "sale")
                {
                    discountamount = RetailPrice - SalePrice;
                    Discount = (discountamount / RetailPrice) * 100;
                    if (Discount.ToString() != "NaN")
                        txtDiscount.Text = Discount.ToString();
                }

            }
            catch (Exception)
            {


            }
            return 0.00;
        }

        protected void txtDiscount_TextChanged(object sender, EventArgs e)
        {
            calculateDiscount("disc");
        }

        protected void txtPrice_TextChanged(object sender, EventArgs e)
        {
            calculateDiscount("retail");
        }

        protected void txtSalePrice_TextChanged(object sender, EventArgs e)
        {
            calculateDiscount("sale");
        }

        protected void SingleButton1_Click(object sender, EventArgs e)
        {
            if(SingleUploadImages.HasFiles)
            {
                string imgString = "";
                string FileNames = "";
                foreach (HttpPostedFile uploadedFile in SingleUploadImages.PostedFiles)
                {
                    uploadedFile.SaveAs(System.IO.Path.Combine(Server.MapPath("~/Images/"), uploadedFile.FileName));
                    imgString += "<img src='Images/" + String.Format("{0}<br />", uploadedFile.FileName + "' />");
                    if (FileNames != "")
                    {
                        FileNames = FileNames + "," + uploadedFile.FileName;
                    }
                    else
                    {
                        FileNames = FileNames + uploadedFile.FileName;
                    }
                }
                div1SingleUploadImages.InnerHtml = imgString;
                SinglelblFileNames.Text = FileNames;
                dalItem.PrimaryFileName = FileNames;
            }

            }

        protected void btnCancel_Click1(object sender, EventArgs e)
        {
            ClearText();
                
       
        }
    }
    }


