﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BAL;
using System.Data;

namespace OnlineShopping
{
    public partial class Master : System.Web.UI.MasterPage
    {
        public static string UserName = "";
        public static string UserId = "";

        DALItemMaster dalItem = new DALItemMaster();
        BALItemMaster balItem = new BALItemMaster();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public Button SendbtnSaveCat
        {
            get { return btnSaveCat; }
            set { btnSaveCat = value; }
        }

        public TextBox SendCatNameText
        {
            get { return txtAddCatName; }
            set { txtAddCatName = value; }
        }

        public Button SendbtnSaveStock
        {
            get { return btnSaveStock; }
            set { btnSaveStock = value; }
        }
  
        public TextBox SendQuantityText
        {
            get { return txtStockQuantity; }
            set { txtStockQuantity = value; }
        }
        public TextBox SendStockPurchaseDate
        {
            get { return txtStockPurchaseDate; }
            set { txtStockPurchaseDate = value; }
        }

        public RadioButton SendrdbAddStock
        {
            get { return rdbAddStock; }
            set { rdbAddStock = value; }
        }

        public RadioButton SendrdbRemoveStock
        {
            get { return rdbRemoveStock; }
            set { rdbRemoveStock = value; }
        }



        public void setActiveTab(string tabName)
        {
            if (tabName == "addNewProduct")
            {
                liAddNewProduct.Attributes.Add("class", "active");
                liInventory.Attributes.Remove("class");
            }
        }

        public void ActiveTab (string tabName)
        {
            if (tabName == "inventory")
            {
                liInventory.Attributes.Add("class", "active");
                liAddNewProduct.Attributes.Remove("class");
            }
        }

        protected void rdbAddStock_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbAddStock.Checked == true)
            {
                txtStockPurchaseDate.Visible = true;
            }
            else
            {
                txtStockPurchaseDate.Visible = false;
            }
        }
    }
}