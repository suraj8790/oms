﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="OnlineShopping.UI.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Mobile Shoppee</title>
    <link href="img/favicon.png" rel="shortcut icon" />
    <link rel="stylesheet" href="../content/css/bootstrap/bootstrap.min.css" />
    <link rel="stylesheet" href="../plugins/bootstrap/css/bootstrap-slider.css" />
    <link href="../content/css/style.css" rel="stylesheet" />
</head>
<body class="body-wrapper">
    <form id="form1" runat="server">
        <div class="login-wrapper">
            <div class="loginform">

                <div class="form-group">
                    <label for="username">User Name</label>
                    <asp:TextBox runat="server" ID="txtUserName" class="form-control" placeholder="Username" required="true"></asp:TextBox>
                    <%--<asp:input type="text" class="form-control" id="username" aria-describedby="emailHelp" placeholder="Enter Username">--%>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <asp:TextBox runat="server" ID="txtPassword" type="password" class="form-control" placeholder="Password" required="true"></asp:TextBox>
                    <%--<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">--%>
                </div>
                <div class="text-right mt-2">
                    <asp:Button runat="server" ID="btnSignIn" class="btn btn-primary" type="submit" Text="SIGN IN" OnClick="btnSignIn_Click" autopostback="true"></asp:Button>

                </div>

            </div>
        </div>
    </form>
    
    <script src="../content/js/jquery/jquery-3.5.1.min.js"></script>
    <script src="../content/js/bootstrap/bootstrap.min.js"></script>
    <script src="../content/js/custom.js"></script>
</body>
</html>
