﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="testWebSvc.aspx.cs" Inherits="OnlineShopping.testWebSvc" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function getData() {
            $.ajax({
                url: "dataprovider.asmx/GetCategoryNamesObj",
                method: "post",
                contenttype: "application/json;charset=utf-8",
                data: json.stringify({ term: document.getElementById("txtId").value }),
                datatype: 'json',
                success: function (data) {
                    responce(data.d);
                    document.getElementById("OutPut").innerText = JSON.stringify(data.d);
                },
                error: function (err) {
                    alert(err);
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <section class="admin" id="AddNewProduct" style="display: block">
                This is test HTML File
        <input type="text" id="txtId" />
                <button id="getDataFromSvc" onclick="getData()">Get Data</button>
                <span id="OutPut"></span>
            </section>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
