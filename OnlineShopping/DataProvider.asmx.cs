﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using DAL;
using BAL;
using System.ServiceModel;

namespace OnlineShopping
{
    /// <summary>
    /// Summary description for DataProvider
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class DataProvider : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public List<categories> GetCategoryNamesObj(string term)
        {
            List<categories> listCountryName = new List<categories>();
            string CS = ConfigurationManager.ConnectionStrings["DBCON"].ConnectionString;
            using (SqlConnection con = new SqlConnection(CS))
            {
                SqlCommand cmd = new SqlCommand("getCategoryList", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter = new SqlParameter()
                {
                    ParameterName = "@catName",
                    Value = term
                };
                cmd.Parameters.Add(parameter);
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    categories cats = new categories();
                    cats.CatId = rdr["CatId"].ToString();
                    cats.CatName = rdr["CatName"].ToString();
                    cats.catCount = rdr["catCount"].ToString();
                    listCountryName.Add(cats);
                }
                return listCountryName;
            }
        }

        [WebMethod]
        public List<string> GetCategoryNames(string term)
        {
            List<string> listCountryName = new List<string>();
            string CS = ConfigurationManager.ConnectionStrings["DBCON"].ConnectionString;
            using (SqlConnection con = new SqlConnection(CS))
            {
                SqlCommand cmd = new SqlCommand("getCategoryList", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter = new SqlParameter()
                {
                    ParameterName = "@catName",
                    Value = term
                };
                cmd.Parameters.Add(parameter);
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    categories cats = new categories();
                    cats.CatId = rdr["CatId"].ToString();
                    cats.CatName = rdr["CatName"].ToString();

                    listCountryName.Add(cats.CatName);
                }
                return listCountryName;
            }
        }

        [WebMethod]
        public List<string> getAllProductsBySearch(string term, string CatId)
        {
            DALItemMaster dalItem = new DALItemMaster();
            BALItemMaster balItem = new BALItemMaster();
            dalItem.DisplayName = term;
            dalItem.CategoryId = Convert.ToInt32(CatId);
            dalItem.Flag = "search";
            List<string> listProductName = new List<string>();
            DataTable dt = balItem.getItems(dalItem);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    listProductName.Add(dt.Rows[i]["DisplayName"].ToString());
                }
            }

            return listProductName;
        }




        [WebMethod]
        public Int32 insertCustomer(String CustId, String CustName, String BillingAddres1, String BillingAddres2, String LandMark1, String PinCode1, String ShipingAddres1, String ShipingAddres2, String LandMark2, String PinCode2, String MobileNo, String Email, String Password)
        {
            DALCustomerMaster dalCustomer = new DALCustomerMaster();
            BALCustomerMaster balCustomer = new BALCustomerMaster();
            dalCustomer.CustId = Convert.ToInt32(CustId);
            dalCustomer.CustName = CustName;
            dalCustomer.BillingAddres1 = BillingAddres1;
            dalCustomer.BillingAddres1 = BillingAddres2;
            dalCustomer.LandMark1 = LandMark1;
            dalCustomer.PinCode1 = PinCode1;
            dalCustomer.ShipingAddres1 = ShipingAddres1;
            dalCustomer.ShipingAddres2 = ShipingAddres2;
            dalCustomer.LandMark2 = LandMark2;
            dalCustomer.PinCode2 = PinCode2;
            dalCustomer.MobileNo = MobileNo;
            dalCustomer.Email = Email;
            dalCustomer.Password = Password;

            int count = balCustomer.InsertCustomer(dalCustomer);
            return count;

        }


        [WebMethod]
        public Int32 updateCustomer(String CustId, String CustName, String BillingAddres1, String BillingAddres2, String LandMark1, String Pincode1, String ShipingAddres1, String ShipingAddres2, String LandMark2, String PinCode2, String MobileNo, String Email, String Password)
        {
            DALCustomerMaster dalCustomer = new DALCustomerMaster();
            BALCustomerMaster balCustomer = new BALCustomerMaster();
            dalCustomer.CustId = Convert.ToInt32(CustId);
            dalCustomer.CustName = CustName;
            dalCustomer.BillingAddres1 = BillingAddres1;
            dalCustomer.BillingAddres1 = BillingAddres2;
            dalCustomer.LandMark1 = LandMark1;
            dalCustomer.PinCode1 = Pincode1;
            dalCustomer.ShipingAddres1 = ShipingAddres1;
            dalCustomer.ShipingAddres2 = ShipingAddres2;
            dalCustomer.LandMark2 = LandMark2;
            dalCustomer.PinCode2 = PinCode2;
            dalCustomer.MobileNo = MobileNo;
            dalCustomer.Email = Email;
            dalCustomer.Password = Password;

            int count = balCustomer.UpdateCustomer(dalCustomer);
            return count;
        }
        private DataTable PrepareDetailDT()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("ItemId");
            dt.Columns.Add("Qauntity");
            dt.Columns.Add("Rate");
            dt.Columns.Add("DiscountAmount");
            dt.Columns.Add("FinalPrice");
            
            return dt;

        }
        [WebMethod]
        public Int32 insertOrder(String CustId, String OrderId, String CustName, 
            String BillingAddres1, String BillingAddres2, String LandMark1, String PinCode1, 
            String ShipingAddres1, String ShipingAddres2, String LandMark2, String PinCode2, 
            String Email, String Password, String TotalPrice, String DiscountAmount, 
            String NetAmount, String UnderStatus, String OrderDate, String OrderUpdateDate, 
            String stDetailXml,String AddressType)
        {
            DALOrderMaster dalOrder = new DALOrderMaster();
            BALOrderMaster balOrder = new BALOrderMaster();
            dalOrder.OrderId = Convert.ToInt32(OrderId);
            dalOrder.CustomerId = Convert.ToInt32(CustId);
            dalOrder.TotalPrice = Convert.ToDecimal(TotalPrice);
            dalOrder.DiscountAmount = Convert.ToDecimal(DiscountAmount);
            dalOrder.NetAmount = Convert.ToDecimal(NetAmount);
            dalOrder.UnderStatus = UnderStatus;
            dalOrder.OrderDate = OrderDate;
            dalOrder.OrderUpdateDate = OrderUpdateDate;
            dalOrder.AddressType = AddressType;
            dalOrder.stDetailXml = stDetailXml;

            //dynamic data = JObject.Parse(stDetailXml);

            List<DALItemMaster> itmList = JsonConvert.DeserializeObject<List<DALItemMaster>>(stDetailXml);
            DataTable dt = new DataTable();
            dt = PrepareDetailDT();

            for (int i = 0; i < itmList.Count; i++)
            {
                var item = itmList[i];
                DataRow Dr = dt.NewRow();

                //Dr[0] = drpGroup.SelectedItem.Text.Trim();
                Dr[0] = item.id;
                Dr[1] = 1;
                Dr[2] = item.RetailPrice;
                Dr[3] = item.Discount;
                Dr[4] = item.SalePrice;
                dt.Rows.Add(Dr);
            }
            string stDetailXmlD = "";
            System.IO.StringWriter sw = new System.IO.StringWriter();
            dt.TableName = "dt";
            dt.WriteXml(sw, XmlWriteMode.IgnoreSchema);
            stDetailXmlD = Convert.ToString(sw);
            dalOrder.stDetailXml = stDetailXmlD;

            DALCustomerMaster dalCustomerMaster = new DALCustomerMaster();
            BALCustomerMaster balCustomerMaster = new BALCustomerMaster();
            dalCustomerMaster.CustName = CustName;
            dalCustomerMaster.CustId = Convert.ToInt32(CustId);
            dalCustomerMaster.BillingAddres1 = BillingAddres1;
            dalCustomerMaster.BillingAddres2 = BillingAddres2;
            dalCustomerMaster.LandMark1 = LandMark1;
            dalCustomerMaster.PinCode1 = PinCode1;
            dalCustomerMaster.ShipingAddres1 = ShipingAddres1;
            dalCustomerMaster.ShipingAddres2 = ShipingAddres2;
            dalCustomerMaster.LandMark2 = LandMark2;
            dalCustomerMaster.PinCode2 = PinCode2;

            int count1 = balCustomerMaster.UpdateCustomer(dalCustomerMaster);
            int count = balOrder.InsertOrder(dalOrder);

            return count;

        }

        [WebMethod]
        public Int32 updateOrder(String OrderId, String CustomerId, String TOtalPrice, String DiscountAmount, String NetAmount, String UnderStatus, String OrderDate, String OrderUpdateDate, String stDetailXml)
        {
            DALOrderMaster dalOrder = new DALOrderMaster();
            BALOrderMaster balOrder = new BALOrderMaster();
            dalOrder.OrderId = Convert.ToInt32(OrderId);
            dalOrder.CustomerId = Convert.ToInt32(CustomerId);
            dalOrder.CustomerId = Convert.ToInt32(CustomerId);
            dalOrder.DiscountAmount = Convert.ToInt32(DiscountAmount);
            dalOrder.NetAmount = Convert.ToInt32(NetAmount);
            dalOrder.UnderStatus = UnderStatus;
            dalOrder.OrderDate = OrderDate;
            dalOrder.OrderUpdateDate = OrderUpdateDate;
            dalOrder.stDetailXml = stDetailXml;


            int count = balOrder.UpdateOrder(dalOrder);

            return count;
        }

        [WebMethod]
        public List<DALItemMaster> getTrendingProducts(string term)
        {
            DALItemMaster dalItem = new DALItemMaster();
            BALItemMaster balItem = new BALItemMaster();
            dalItem.Flag = "GetTrending";

            List<DALItemMaster> listItems = new List<DALItemMaster>();

            DataTable dt = balItem.getItemsForOnline(dalItem);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DALItemMaster item = new DALItemMaster();
                    item.Brand = dt.Rows[i]["Brand"].ToString();
                    item.CategoryId = Convert.ToInt32(dt.Rows[i]["CatgoryId"].ToString());
                    item.CategoryName = dt.Rows[i]["CategoryName"].ToString();
                    item.Colors = dt.Rows[i]["Colors"].ToString();
                    item.Discount = Convert.ToDecimal(dt.Rows[i]["Discount"].ToString());
                    item.DisplayName = dt.Rows[i]["DisplayName"].ToString();
                    item.Flag = "";
                    item.FromDate = "";
                    item.id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    item.Images = dt.Rows[i]["Images"].ToString();
                    item.IsDeleted = false;
                    item.IsTrending = Convert.ToBoolean(dt.Rows[i]["IsTrending"].ToString());
                    item.ItemDetail = dt.Rows[i]["ItemDetails"].ToString();
                    item.Manufacture = dt.Rows[i]["Manufacture"].ToString();
                    item.ModelName = dt.Rows[i]["ModelName"].ToString();
                    item.PQty = 0;
                    item.PrimaryFileName = dt.Rows[i]["PrimaryFileName"].ToString();
                    item.PurchaseDate = "";
                    item.PurchasePrice = Convert.ToDecimal(dt.Rows[i]["PurchasePrice"].ToString());
                    item.RetailPrice = Convert.ToDecimal(dt.Rows[i]["RetailPrice"].ToString());
                    item.SalePrice = Convert.ToDecimal(dt.Rows[i]["SalePrice"].ToString());
                    item.SerialNos = "";
                    item.ShowOnLive = true;
                    item.Stock = Convert.ToDecimal(dt.Rows[i]["Stock"].ToString());
                    item.Warranty = dt.Rows[i]["Warranty"].ToString();

                    listItems.Add(item);

                }
            }

            return listItems;
        }

        [WebMethod]
        public List<DALItemMaster> GetProductsById(string term)
        {
            DALItemMaster dalItem = new DALItemMaster();
            BALItemMaster balItem = new BALItemMaster();
            dalItem.Flag = "GetItembyid";
            dalItem.id = Convert.ToInt32(term);

            List<DALItemMaster> listItems = new List<DALItemMaster>();

            DataTable dt = balItem.getItemsForOnline(dalItem);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DALItemMaster item = new DALItemMaster();
                    item.Brand = dt.Rows[i]["Brand"].ToString();
                    item.CategoryId = Convert.ToInt32(dt.Rows[i]["CatgoryId"].ToString());
                    item.CategoryName = dt.Rows[i]["CategoryName"].ToString();
                    item.Colors = dt.Rows[i]["Colors"].ToString();
                    item.Discount = Convert.ToDecimal(dt.Rows[i]["Discount"].ToString());
                    item.DisplayName = dt.Rows[i]["DisplayName"].ToString();
                    item.Flag = "";
                    item.FromDate = "";
                    item.id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    item.Images = dt.Rows[i]["Images"].ToString();
                    item.IsDeleted = false;
                    item.IsTrending = Convert.ToBoolean(dt.Rows[i]["IsTrending"].ToString());
                    item.ItemDetail = dt.Rows[i]["ItemDetails"].ToString();
                    item.Manufacture = dt.Rows[i]["Manufacture"].ToString();
                    item.ModelName = dt.Rows[i]["ModelName"].ToString();
                    item.PQty = 0;
                    item.PrimaryFileName = dt.Rows[i]["PrimaryFileName"].ToString();
                    item.PurchaseDate = "";
                    item.PurchasePrice = Convert.ToDecimal(dt.Rows[i]["PurchasePrice"].ToString());
                    item.RetailPrice = Convert.ToDecimal(dt.Rows[i]["RetailPrice"].ToString());
                    item.SalePrice = Convert.ToDecimal(dt.Rows[i]["SalePrice"].ToString());
                    item.SerialNos = "";
                    item.ShowOnLive = true;
                    item.Stock = Convert.ToDecimal(dt.Rows[i]["Stock"].ToString());
                    item.Warranty = dt.Rows[i]["Warranty"].ToString();


                    listItems.Add(item);
                }
            }
            return listItems;
        }

        [WebMethod]
        public List<DALItemMaster> GetProductsByCategoryId(string term)
        {
            DALItemMaster dalItem = new DALItemMaster();
            BALItemMaster balItem = new BALItemMaster();
            dalItem.Flag = "GetItemsByCategary";
            dalItem.CategoryId = Convert.ToInt32(term);

            List<DALItemMaster> listItems = new List<DALItemMaster>();

            DataTable dt = balItem.getItemsForOnline(dalItem);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DALItemMaster item = new DALItemMaster();
                    item.Brand = dt.Rows[i]["Brand"].ToString();
                    item.CategoryId = Convert.ToInt32(dt.Rows[i]["CatgoryId"].ToString());
                    item.CategoryName = dt.Rows[i]["CategoryName"].ToString();
                    item.Colors = dt.Rows[i]["Colors"].ToString();
                    item.Discount = Convert.ToDecimal(dt.Rows[i]["Discount"].ToString());
                    item.DisplayName = dt.Rows[i]["DisplayName"].ToString();
                    item.Flag = "";
                    item.FromDate = "";
                    item.id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    item.Images = dt.Rows[i]["Images"].ToString();
                    item.IsDeleted = false;
                    item.IsTrending = Convert.ToBoolean(dt.Rows[i]["IsTrending"].ToString());
                    item.ItemDetail = dt.Rows[i]["ItemDetails"].ToString();
                    item.Manufacture = dt.Rows[i]["Manufacture"].ToString();
                    item.ModelName = dt.Rows[i]["ModelName"].ToString();
                    item.PQty = 0;
                    item.PrimaryFileName = dt.Rows[i]["PrimaryFileName"].ToString();
                    item.PurchaseDate = "";
                    item.PurchasePrice = Convert.ToDecimal(dt.Rows[i]["PurchasePrice"].ToString());
                    item.RetailPrice = Convert.ToDecimal(dt.Rows[i]["RetailPrice"].ToString());
                    item.SalePrice = Convert.ToDecimal(dt.Rows[i]["SalePrice"].ToString());
                    item.SerialNos = "";
                    item.ShowOnLive = true;
                    item.Stock = Convert.ToDecimal(dt.Rows[i]["Stock"].ToString());
                    item.Warranty = dt.Rows[i]["Warranty"].ToString();


                    listItems.Add(item);
                }
            }
            return listItems;
        }

        [WebMethod]
        public List<DALItemMaster> GetProductsinPriceRange(decimal price)
        {
            DALItemMaster dalItem = new DALItemMaster();
            BALItemMaster balItem = new BALItemMaster();
            dalItem.Flag = "GetTrending";

            List<DALItemMaster> listItem = new List<DALItemMaster>();
            DataTable dt = balItem.getItemsForOnline(dalItem);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DALItemMaster item = new DALItemMaster();
                    item.Brand = dt.Rows[i]["Brand"].ToString();
                    item.CategoryId = Convert.ToInt32(dt.Rows[i]["CategoryId"].ToString());
                    item.CategoryName = dt.Rows[i]["CategoryName"].ToString();
                    item.Colors = dt.Rows[i]["Colors"].ToString();
                    item.Discount = Convert.ToDecimal(dt.Rows[i]["Discount"].ToString());
                    item.DisplayName = dt.Rows[i]["DisplyaName"].ToString();
                    item.Flag = "";
                    item.FromDate = "";
                    item.id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    item.Images = dt.Rows[i]["Images"].ToString();
                    item.IsDeleted = false;
                    item.IsTrending = Convert.ToBoolean(dt.Rows[i]["IsTrending"].ToString());
                    item.ItemDetail = dt.Rows[i]["ItemDetail"].ToString();
                    item.Manufacture = dt.Rows[i]["Manufacture"].ToString();
                    item.ModelName = dt.Rows[i]["ModelName"].ToString();
                    item.PQty = 0;
                    item.PrimaryFileName = dt.Rows[i]["PrimaryFileName"].ToString();
                    item.PurchaseDate = "";
                    item.PurchasePrice = Convert.ToDecimal(dt.Rows[i]["PurchasePrice"].ToString());
                    item.RetailPrice = Convert.ToDecimal(dt.Rows[i]["RetailPrice"].ToString());
                    item.SalePrice = Convert.ToDecimal(dt.Rows[i]["SalePrice"].ToString());
                    item.SerialNos = "";
                    item.ShowOnLive = true;
                    item.Stock = Convert.ToDecimal(dt.Rows[i]["Stock"].ToString());
                    item.Warranty = dt.Rows[i]["Warranty"].ToString();

                    listItem.Add(item);
                }
            }
            return listItem;
        }

        [WebMethod]
        public DALCustomerMaster loginCustomer(string mobileno, string password)
        {
            DALCustomerMaster dalCust = new DALCustomerMaster();
            BALCustomerMaster balCust = new BALCustomerMaster();
            dalCust.Flag = "Login";
            dalCust.MobileNo = mobileno;
            dalCust.Password = password;


            DataTable dt = balCust.getCustomerLogin(dalCust);
            DALCustomerMaster item = new DALCustomerMaster();
            if (dt.Rows.Count > 0)
            {
                item.CustId = Convert.ToInt32(dt.Rows[0]["CustId"].ToString());
                item.CustItems = Convert.ToInt32(dt.Rows[0]["CustItems"].ToString());
                item.CustName = dt.Rows[0]["CustName"].ToString();
                item.MobileNo = dt.Rows[0]["MobileNo"].ToString();
                item.Password = "";
                item.PinCode1 = dt.Rows[0]["PinCode1"].ToString();
                item.Email = dt.Rows[0]["Email"].ToString();
                item.BillingAddres1 = dt.Rows[0]["BillingAddres1"].ToString();
                item.BillingAddres2 = dt.Rows[0]["BillingAddres2"].ToString();
                item.LandMark1 = dt.Rows[0]["LandMark1"].ToString();
                item.ShipingAddres1 = dt.Rows[0]["ShipingAddres1"].ToString();
                item.ShipingAddres2 = dt.Rows[0]["ShipingAddres2"].ToString();
                item.LandMark2 = dt.Rows[0]["LandMark2"].ToString();
                item.PinCode2 = dt.Rows[0]["PinCode2"].ToString();

            }
            return item;
        }


        public class categories
        {
            public string CatId { get; set; }
            public string CatName { get; set; }
            public string catCount { get; set; }
        }

        public class itemList
        {
            public List<DALItemMaster> items;

            public List<DALItemMaster> Items { get; set; }
        }
    }
}
