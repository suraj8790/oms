USE [master]
GO
/****** Object:  Database [OnlineShop]    Script Date: 10/19/2020 12:22:52 ******/
CREATE DATABASE [OnlineShop] ON  PRIMARY 
( NAME = N'OnlineShop', FILENAME = N'c:\Program Files (x86)\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\OnlineShop.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'OnlineShop_log', FILENAME = N'c:\Program Files (x86)\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\OnlineShop_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [OnlineShop] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [OnlineShop].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [OnlineShop] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [OnlineShop] SET ANSI_NULLS OFF
GO
ALTER DATABASE [OnlineShop] SET ANSI_PADDING OFF
GO
ALTER DATABASE [OnlineShop] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [OnlineShop] SET ARITHABORT OFF
GO
ALTER DATABASE [OnlineShop] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [OnlineShop] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [OnlineShop] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [OnlineShop] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [OnlineShop] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [OnlineShop] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [OnlineShop] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [OnlineShop] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [OnlineShop] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [OnlineShop] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [OnlineShop] SET  DISABLE_BROKER
GO
ALTER DATABASE [OnlineShop] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [OnlineShop] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [OnlineShop] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [OnlineShop] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [OnlineShop] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [OnlineShop] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [OnlineShop] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [OnlineShop] SET  READ_WRITE
GO
ALTER DATABASE [OnlineShop] SET RECOVERY SIMPLE
GO
ALTER DATABASE [OnlineShop] SET  MULTI_USER
GO
ALTER DATABASE [OnlineShop] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [OnlineShop] SET DB_CHAINING OFF
GO
USE [OnlineShop]
GO
/****** Object:  StoredProcedure [dbo].[CustomerLogin]    Script Date: 10/19/2020 12:22:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kiran Naik>
-- Create date: <17-10-2020>
-- Description:	<Customer Login>
-- =============================================
CREATE PROCEDURE [dbo].[CustomerLogin]
(
@MobileNo varchar(50) =null,
@Password varchar(50)= null,
@Flag varchar(50)=null
)
AS
BEGIN
 
 BEGIN TRY
   BEGIN TRANSACTION
   
   IF(@Flag = 'Login')
BEGIN
     SELECT
     CustId,
     CustName,
     BillingAddres,
     PinCode,
     MobileNo,
     Email,
     CustItems,
     Password
     FROM CustomerLogin WHERE MobileNo = @MobileNo AND Password = @Password;
END

 COMMIT TRANSACTION
END TRY
BEGIN CATCH
    DECLARE 
        @ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();
    RAISERROR (
        @ErrorMessage,
        @ErrorSeverity,
        @ErrorState    
        );
    ROLLBACK TRANSACTION
END CATCH
END
GO
/****** Object:  Table [dbo].[CustomerCart]    Script Date: 10/19/2020 12:22:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerCart](
	[CustomerId] [int] NULL,
	[ItemId] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CategoryMaster]    Script Date: 10/19/2020 12:22:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CategoryMaster](
	[CatId] [int] IDENTITY(1,1) NOT NULL,
	[CatName] [varchar](50) NULL,
	[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ItemMaster]    Script Date: 10/19/2020 12:22:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ItemMaster](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Manufacture] [varchar](100) NULL,
	[Brand] [varchar](50) NULL,
	[ModelName] [varchar](50) NULL,
	[SerialNos] [varchar](200) NULL,
	[Warranty] [varchar](50) NULL,
	[PurchaseDate] [varchar](10) NULL,
	[PurchasePrice] [numeric](18, 2) NULL,
	[PQty] [int] NOT NULL,
	[RetailPrice] [numeric](18, 2) NULL,
	[Discount] [numeric](18, 2) NULL,
	[SalePrice] [numeric](18, 2) NULL,
	[ShowOnLive] [bit] NULL,
	[DisplayName] [varchar](150) NULL,
	[CatgoryId] [int] NULL,
	[Colors] [varchar](100) NULL,
	[ItemDetails] [varchar](500) NULL,
	[Stock] [numeric](18, 2) NULL,
	[Images] [varchar](800) NULL,
	[IsTrending] [bit] NULL,
	[PrimaryFileName] [varchar](50) NULL,
	[IsDeleted] [bit] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[InsertUpdateOrder_Master]    Script Date: 10/19/2020 12:22:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kiran Naik>
-- Create date: <30-9-2020>
-- Description:	<InsertUpdateOrder_Master>
-- =============================================
CREATE PROCEDURE [dbo].[InsertUpdateOrder_Master]
(
@OrderId int = 0,
@CustomerId int = 0,
@TotalPrice numeric(18, 2)=null,
@DiscountAmount numeric(18, 2)=null,
@NetAmount numeric(18, 2)=null,
@UnderStatus varchar(10) =null,
@OrderDate varchar(10)=null,
@OrderUpdateDate varchar(10) = null
)

AS 
BEGIN

BEGIN TRY
 BEGIN TRANSACTION
 IF(@OrderId = 0 OR @OrderId = null)
 BEGIN
 INSERT INTO InsertUpdateOrder_Master
 (
 
 CustomerId,
 TotalPrice,
 DiscountAmount,
 NetAmount,
 UnderStatus,
 OrderDate,
 OrderUpdateDate,
 IsDeleted
 )
 VALUES(

 @CustomerId,
 @TotalPrice,
 @DiscountAmount,
 @NetAmount,
 @OrderDate,
 @OrderUpdateDate,
 @UnderStatus,
 0
 );
 END
	
	ELSE 
BEGIN
	UPDATE InsertUpdateOrder_Master SET
	
	CustomerId = @CustomerId,
	TotalPrice = @TotalPrice,
	DiscountAmount =@DiscountAmount,
	NetAmount=@NetAmount,
	UnderStatus=@UnderStatus,
	OrderDate=@OrderDate,
	OrderUpdateDate=@OrderUpdateDate
	WHERE
	OrderId =@OrderId;
END
	 COMMIT TRANSACTION
  END TRY
BEGIN CATCH
    DECLARE 
        @ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();
    RAISERROR (
        @ErrorMessage,
        @ErrorSeverity,
        @ErrorState    
        );
    ROLLBACK TRANSACTION
END CATCH
END
GO
/****** Object:  Table [dbo].[PurchaseDetails]    Script Date: 10/19/2020 12:22:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PurchaseDetails](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Podate] [varchar](10) NULL,
	[Qty] [numeric](18, 2) NULL,
	[Itemid] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrderDetails]    Script Date: 10/19/2020 12:22:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetails](
	[OrderId] [int] NULL,
	[ItemId] [int] NULL,
	[Qauntity] [int] NULL,
	[Rate] [decimal](18, 2) NULL,
	[DiscountAmount] [decimal](18, 2) NULL,
	[FinalPrice] [decimal](18, 2) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order_Master]    Script Date: 10/19/2020 12:22:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Order_Master](
	[OrderId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NULL,
	[TotalPrice] [numeric](18, 2) NULL,
	[DiscountAmount] [numeric](18, 2) NULL,
	[NetAmount] [numeric](18, 2) NULL,
	[UnderStatus] [varchar](10) NULL,
	[IsDeleted] [bit] NULL,
	[OrderDate] [varchar](10) NULL,
	[OrderUpdateDate] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CustomerMaster]    Script Date: 10/19/2020 12:22:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomerMaster](
	[CustId] [int] NULL,
	[CustName] [varchar](100) NULL,
	[BillingAddres] [varchar](300) NULL,
	[PinCode] [varchar](6) NULL,
	[MobileNo] [varchar](13) NULL,
	[Email] [varchar](100) NULL,
	[CustItems] [int] NULL,
	[Password] [varchar](50) NULL,
	[IsDeleted] [bit] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[CustomerLogin2]    Script Date: 10/19/2020 12:22:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kiran Naik>
-- Create date: <17-10-2020>
-- Description:	<Customer Login>
-- =============================================
CREATE PROCEDURE [dbo].[CustomerLogin2]
(
@MobileNo varchar(50) =null,
@Password varchar(50)= null,
@Flag varchar(50)=null
)
AS
BEGIN
 
 BEGIN TRY
   BEGIN TRANSACTION
   
   IF(@Flag = 'Login')
BEGIN
     SELECT
     CustId,
     CustName,
     BillingAddres,
     PinCode,
     MobileNo,
     Email,
     CustItems,
     Password
     FROM CustomerLogin2 WHERE MobileNo = @MobileNo AND Password = @Password;
END

 COMMIT TRANSACTION
END TRY
BEGIN CATCH
    DECLARE 
        @ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();
    RAISERROR (
        @ErrorMessage,
        @ErrorSeverity,
        @ErrorState    
        );
    ROLLBACK TRANSACTION
END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[CustomerLogin1]    Script Date: 10/19/2020 12:22:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kiran Naik>
-- Create date: <17-10-2020>
-- Description:	<Customer Login>
-- =============================================
CREATE PROCEDURE [dbo].[CustomerLogin1]
(
@MobileNo varchar(50) =null,
@Password varchar(50)= null,
@Flag varchar(50)=null
)
AS
BEGIN
 
 BEGIN TRY
   BEGIN TRANSACTION
   
   IF(@Flag = 'Login')
BEGIN
     SELECT
     CustId,
     CustName,
     BillingAddres,
     PinCode,
     MobileNo,
     Email,
     CustItems,
     Password
     FROM CustomerLogin1 WHERE MobileNo = @MobileNo AND Password = @Password;
END

 COMMIT TRANSACTION
END TRY
BEGIN CATCH
    DECLARE 
        @ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();
    RAISERROR (
        @ErrorMessage,
        @ErrorSeverity,
        @ErrorState    
        );
    ROLLBACK TRANSACTION
END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[Select_OrderMaster]    Script Date: 10/19/2020 12:22:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kiran Naik>
-- Create date: <1-10-2020>
-- Description:	<Select_OrderMaster>
-- =============================================
CREATE PROCEDURE  [dbo].[Select_OrderMaster]
(
@OrderId int=0,
@CustomerId int =0,
@Flag varchar =null,
@FromDate varchar(10)=null,
@ToDate varchar(10)=null
)
AS
BEGIN

  BEGIN TRY
     BEGIN TRANSACTION
     IF(@Flag ='FillGrid')
     BEGIN
       SELECT
          OrderId,
          CustomerId,
          TotalPrice,
          DiscountAmount,
          NetAmount,
          UnderStatus,
          OrderDate,
          OrderUpdateDate
       FROM Order_Master WHERE IsDeleted=0;
       END
       
       IF(@Flag='SelectOne')
       BEGIN 
          SELECT
            OrderId,
            CustomerId,
            TotalPrice,
            DiscountAmount,
            NetAmount,
            UnderStatus,
            OrderDate,
            OrderUpdateDate
          FROM Order_Master WHERE IsDeleted=0 AND OrderId=@OrderId;
       END
       
       COMMIT TRANSACTION
END TRY
BEGIN CATCH
    DECLARE 
        @ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();
    RAISERROR (
        @ErrorMessage,
        @ErrorSeverity,
        @ErrorState    
        );
    ROLLBACK TRANSACTION
END CATCH

	 
END
GO
/****** Object:  StoredProcedure [dbo].[Select_ItemMaster]    Script Date: 10/19/2020 12:22:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Suraj Desai>
-- Create date: <21-09-2020>
-- Description:	<To Insert Update ItemMaster>
-- =============================================
CREATE PROCEDURE [dbo].[Select_ItemMaster] 
	(
	@id	int =0,
	@ItemName varchar(150)=null,
	@Flag varchar(50) =null,
	@FromDate varchar(20)=null,
	@ToDate varchar(20)=null
	)
AS
BEGIN

BEGIN TRY
    BEGIN TRANSACTION
    
     IF(@Flag ='FillGrid')
 BEGIN
		 SELECT id,
		Manufacture,
		Brand,
		ModelName,
		SerialNos,
		Warranty,
		PurchaseDate,
		PurchasePrice,
		PQty,
		RetailPrice,
		Discount,
		SalePrice,
		ShowOnLive,
		DisplayName,
		CatgoryId,
		Colors,
		ItemDetails,
		Stock,
		Images
		IsTrending,
		PrimaryFileName
		FROM ItemMaster WHERE IsDeleted = 0;

 END
    IF(@Flag ='search')
 BEGIN
		 SELECT id,
		Manufacture,
		Brand,
		ModelName,
		SerialNos,
		Warranty,
		PurchaseDate,
		PurchasePrice,
		PQty,
		RetailPrice,
		Discount,
		SalePrice,
		ShowOnLive,
		DisplayName,
		CatgoryId,
		Colors,
		ItemDetails,
		Stock,
		Images,
		IsTrending,
		PrimaryFileName
		FROM ItemMaster WHERE IsDeleted = 0 AND DisplayName LIKE '%'+@ItemName+'%';

 END
 
    IF(@Flag ='SelectOne')
 BEGIN
		 SELECT id,
		Manufacture,
		Brand,
		ModelName,
		SerialNos,
		Warranty,
		PurchaseDate,
		PurchasePrice,
		PQty,
		RetailPrice,
		Discount,
		SalePrice,
		ShowOnLive,
		DisplayName,
		CatgoryId,
		Colors,
		ItemDetails,
		Stock,
		Images,
		IsTrending,
		PrimaryFileName
		FROM ItemMaster WHERE IsDeleted = 0 AND id=@id;

 END
    
    COMMIT TRANSACTION
END TRY
BEGIN CATCH
    DECLARE 
        @ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();
    RAISERROR (
        @ErrorMessage,
        @ErrorSeverity,
        @ErrorState    
        );
    ROLLBACK TRANSACTION
END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[Select_CustomerMaster]    Script Date: 10/19/2020 12:22:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<kiran Naik>
-- Create date: <30-9-2020>
-- Description:	<Select_CustomerMaster>
-- =============================================
CREATE PROCEDURE [dbo].[Select_CustomerMaster]
(
@CustId int=0,
@CustName varchar(100)=null,
@Flag varchar(50)=null
)
AS
BEGIN
 
 BEGIN TRY
  BEGIN TRANSACTION
       IF(@Flag ='FillGrid')
       BEGIN
          SELECT
            CustId,
            CustName,
            BillingAddres,
            Pincode,
            MobileNo,
            Email,
            CustItems
            FROM CustomerMaster WHERE IsDeleted=0;
        END
                    
          IF (@Flag ='SelectOne')    
       BEGIN
           SELECT
              CustId,
            CustName,
            BillingAddres,
            Pincode,
            MobileNo,
            Email,
            CustItems
            FROM CustomerMaster WHERE IsDeleted=0 AND CustId=@CustId;   
         END     
   
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
    DECLARE 
        @ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();
    RAISERROR (
        @ErrorMessage,
        @ErrorSeverity,
        @ErrorState    
        );
    ROLLBACK TRANSACTION
END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[Customer-Login]    Script Date: 10/19/2020 12:22:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kiran Naik>
-- Create date: <17-10-2020>
-- Description:	<Customer Login>
-- =============================================
CREATE PROCEDURE [dbo].[Customer-Login]
(
@MobileNo varchar(50) =null,
@Password varchar(50)= null,
@Flag varchar(50)=null
)
AS
BEGIN
 
 BEGIN TRY
   BEGIN TRANSACTION
   
   IF(@Flag = 'Login')
BEGIN
     SELECT
     CustId,
     CustName,
     BillingAddres,
     PinCode,
     MobileNo,
     Email,
     CustItems,
     Password
     FROM CustomerLogin WHERE MobileNo = @MobileNo AND Password = @Password;
END

 COMMIT TRANSACTION
END TRY
BEGIN CATCH
    DECLARE 
        @ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();
    RAISERROR (
        @ErrorMessage,
        @ErrorSeverity,
        @ErrorState    
        );
    ROLLBACK TRANSACTION
END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[Order_Details]    Script Date: 10/19/2020 12:22:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kiran Naik>
-- Create date: <30-9-2020>
-- Description:	<Order_Details>
-- =============================================
CREATE PROCEDURE [dbo].[Order_Details]
(
@OrderId int= 0,
@ItemId int = 0
)
AS
BEGIN
 
 BEGIN TRY
 BEGIN TRANSACTION
 if(@OrderId=0 OR @OrderId= null)
   BEGIN
  INSERT INTO OrderDetails
  (
  
  ItemId
  )
  VALUES(
 
  @ItemId
  );
  END
  
  ELSE
  BEGIN
  UPDATE OrderDetails SET
	
	ItemId = @ItemId
	WHERE
	OrderId= @OrderId
	END
	
	 COMMIT TRANSACTION
  END TRY
BEGIN CATCH
    DECLARE 
        @ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();
    RAISERROR (
        @ErrorMessage,
        @ErrorSeverity,
        @ErrorState    
        );
    ROLLBACK TRANSACTION
END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[Onlinedataprovider]    Script Date: 10/19/2020 12:22:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<KIRAN>
-- Create date: <29-9-2020>
-- Description:	<mobileshopi>
-- =============================================
CREATE PROCEDURE [dbo].[Onlinedataprovider]
(
    @id	int =0,
	@ItemName varchar(150)=null,
	@Flag varchar(50) =null,
	@FromDate varchar(20)=null,
	@ToDate varchar(20)=null,
	@CatgoryId varchar(10)=null,
	@FromPurchacePrice decimal(18,2)=0.00,
	@ToPurchasePrice decimal(18,2)=0.00
	)
AS
BEGIN
	
	BEGIN TRY
    BEGIN TRANSACTION
   
   
    IF(@Flag ='GetAllItems') 
 BEGIN 
     SELECT id,
		Manufacture,
		Brand,
		ModelName,
		SerialNos,
		Warranty,
		PurchaseDate,
		PurchasePrice,
		PQty,
		RetailPrice,
		Discount,
		SalePrice,
		ShowOnLive,
		DisplayName,
		CatgoryId,
		Colors,
		ItemDetails,
		Stock,
		Images,
		PrimaryFileName,
		IsTrending,
		(SELECT CatName FROM CategoryMaster WHERE CatId =I.CatgoryId) as 'CategoryName'
     FROM ItemMaster I WHERE IsDeleted=0 AND ShowOnLive =1;
     
 END
 
 IF(@Flag ='GetTrending') 
 BEGIN 
     SELECT id,
		Manufacture,
		Brand,
		ModelName,
		SerialNos,
		Warranty,
		PurchaseDate,
		PurchasePrice,
		PQty,
		RetailPrice,
		Discount,
		SalePrice,
		ShowOnLive,
		DisplayName,
		CatgoryId,
		Colors,
		ItemDetails,
		Stock,
		Images,
		PrimaryFileName,
		IsTrending,
		(SELECT CatName FROM CategoryMaster WHERE CatId =I.CatgoryId) as 'CategoryName'
     FROM ItemMaster I WHERE IsDeleted=0 AND ShowOnLive =1 AND IsTrending =1;
     
 END
    
    IF (@Flag ='GetItemsByCategary')
 BEGIN
    SELECT   id,
		Manufacture,
		Brand,
		ModelName,
		SerialNos,
		Warranty,
		PurchaseDate,
		PurchasePrice,
		PQty,
		RetailPrice,
		Discount,
		SalePrice,
		ShowOnLive,
		DisplayName,
		CatgoryId,
		Colors,
		ItemDetails,
		Stock,
		Images,
		PrimaryFileName,
		IsTrending,
		(SELECT CatName FROM CategoryMaster WHERE CatId =I.CatgoryId) as 'CategoryName'
    FROM ItemMaster I WHERE IsDeleted =0 AND ShowOnLive =1 AND CatgoryId =@CatgoryId;
    
 END
 
    IF(@Flag ='GetItembyid')
 BEGIN
    SELECT id,
		Manufacture,
		Brand,
		ModelName,
		SerialNos,
		Warranty,
		PurchaseDate,
		PurchasePrice,
		PQty,
		RetailPrice,
		Discount,
		SalePrice,
		ShowOnLive,
		DisplayName,
		CatgoryId,
		Colors,
		ItemDetails,
		Stock,
		Images,
		PrimaryFileName,
		IsTrending,
		(SELECT CatName FROM CategoryMaster WHERE CatId =I.CatgoryId) as 'CategoryName'
    FROM ItemMaster I WHERE IsDeleted =0 AND ShowOnLive =1 AND id = @id;
 
 END   
 
 
 IF(@Flag='GetProductsById')
 BEGIN
	SELECT id,
		Manufacture,
		Brand,
		ModelName,
		SerialNos,
		Warranty,
		PurchaseDate,
		PurchasePrice,
		PQty,
		RetailPrice,
		Discount,
		SalePrice,
		ShowOnLive,
		DisplayName,
		CatgoryId,
		Colors,
		ItemDetails,
		Stock,
		Images,
		PrimaryFileName,
		IsTrending,
	(SELECT CatName FROM CategoryMaster WHERE CatId=I.CatgoryId) as 'CategoryName'
	FROM ItemMaster I WHERE IsDeleted = 0 AND ShowOnLive = 1 AND id = @id;
	
END

IF(@Flag='GetProductsByCatgoryId')
BEGIN
	SELECT id,
	Manufacture,
	Brand,
	ModelName,
	SerialNos,
	Warranty,
	PurchaseDate,
	PurchasePrice,
	PQty,
	RetailPrice,
	Discount,
	SalePrice,
	ShowOnLive,
	DisplayName,
	CatgoryId,
	Colors,
	ItemDetails,
	Stock,
	Images,
	PrimaryFileName,
	IsTrending,
	(SELECT CatName FROM CategoryMaster WHERE CatId=I.CatgoryId) as 'CategoryName'
	FROM ItemMaster I WHERE IsDeleted = 0 AND ShowOnLive = 1 AND id = @id AND CatgoryId= @CatgoryId;
END

IF (@Flag = 'GetProductsInPriceRange')
BEGIN 
	SELECT id,
	Manufacture,
	Brand,
	ModelName,
	SerialNos,
	Warranty,
	PurchaseDate,
	PurchasePrice,
	PQty,
	RetailPrice,
	Discount,
	SalePrice,
	ShowOnLive,
	DisplayName,
	CatgoryId,
	Colors,
	ItemDetails,
	Stock,
	Images,
	PrimaryFileName,
	IsTrending,
	(SELECT CatName FROM CategoryMaster WHERE CatId=I.CatgoryId) as 'CategoryName'
	FROM ItemMaster I WHERE IsDeleted = 0 AND ShowOnLive = 1 AND id = @id AND PurchasePrice>=@FromPurchacePrice AND PurchasePrice <=@ToPurchasePrice;
	
END
 
    
    
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
    DECLARE 
        @ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();
    RAISERROR (
        @ErrorMessage,
        @ErrorSeverity,
        @ErrorState    
        );
    ROLLBACK TRANSACTION
END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[InsertUpdateCustomerMaster]    Script Date: 10/19/2020 12:22:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kiran Naik>
-- Create date: <29-9-2020>
-- Description:	<InsertUpdateCustomerMaster>
-- =============================================
 CREATE PROCEDURE [dbo].[InsertUpdateCustomerMaster]
 (
 @CustId int = 0,
 @CustName varchar(100)=null,
 @BillingAddres varchar(300)=null,
 @PinCode varchar(6)=null,
 @MobileNo varchar(13)=null,
 @Email varchar(100) =null,
 @CustItems int=0
  )
AS
BEGIN

BEGIN TRY
    BEGIN TRANSACTION
    IF(@CustId=0 OR @CustId=null)
  BEGIN
  INSERT INTO CustomerMaster
  (
 
  CustName,
  BillingAddres,
  PinCode,
  MobileNo,
  Email,
  CustItems,
  IsDeleted
  )
  VALUES (
  
  @CustName,
  @BillingAddres,
  @PinCode,
  @MobileNo,
  @Email,
  @CustItems,
  0
  );
  END
  
  ELSE
  BEGIN
  UPDATE CustomerMaster SET
  
  CustName = @CustName,
  BillingAddres = @BillingAddres,
  PinCode = @PinCode,
  MobileNo = @MobileNo,
  Email = @Email,
  CustItems = @CustItems
  WHERE
  CustId= @CustId;
  END
   COMMIT TRANSACTION
  END TRY
BEGIN CATCH
    DECLARE 
        @ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();
    RAISERROR (
        @ErrorMessage,
        @ErrorSeverity,
        @ErrorState    
        );
    ROLLBACK TRANSACTION
END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[InsertUpdateCategory]    Script Date: 10/19/2020 12:22:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Suraj Desai>
-- Create date: <21-09-2020>
-- Description:	<To Insert Update ItemMaster>
-- =============================================
CREATE PROCEDURE [dbo].[InsertUpdateCategory] 
	(
	@CatId	int =0,
@CatName	varchar(50)=null
	
	)
AS
BEGIN

BEGIN TRY
    BEGIN TRANSACTION
    if(@CatId =0 OR @CatId =null)
    BEGIN
    INSERT INTO CategoryMaster
    (
		CatName,
		IsDeleted
    )VALUES(
        @CatName,
		0
    );
    END
    ELSE
    BEGIN
    UPDATE CategoryMaster SET 
    CatName =@CatName
		WHERE 
		CatId = @CatId;
    END
    COMMIT TRANSACTION
END TRY
BEGIN CATCH
    DECLARE 
        @ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();
    RAISERROR (
        @ErrorMessage,
        @ErrorSeverity,
        @ErrorState    
        );
    ROLLBACK TRANSACTION
END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[InsertUpdate_ItemMaster]    Script Date: 10/19/2020 12:22:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Suraj Desai>
-- Create date: <21-09-2020>
-- Description:	<To Insert Update ItemMaster>
-- =============================================
CREATE PROCEDURE [dbo].[InsertUpdate_ItemMaster] 
	(
@id	int =0,
@Manufacture	varchar(100)=null,
@Brand	varchar(50)=null,
@ModelName	varchar(50)=null,
@SerialNos	varchar(200)=null,
@Warranty	varchar(50)=null,
@PurchaseDate	varchar(10)=null,
@PurchasePrice	numeric(18, 2)=0.00,
@PQty	int=null,
@RetailPrice	numeric(18, 2)=0.00,
@Discount	numeric(18, 2)=0.00,
@SalePrice	numeric(18, 2)=0.00,
@DisplayName varchar(150)=null,
@ShowOnLive	bit=true,
@CatgoryId int = 0,
@Colors varchar(100) =null,
@ItemDetails varchar(500) =null,
@Images varchar(8000)=null,
@IsTrending bit=false,
@PrimaryFileName varchar(50)=null
	)
AS
BEGIN

BEGIN TRY
    BEGIN TRANSACTION
    if(@id =0 OR @id =null)
    BEGIN
    INSERT INTO ItemMaster
    (
		Manufacture,
		Brand,
		ModelName,
		SerialNos,
		Warranty,
		PurchaseDate,
		PurchasePrice,
		PQty,
		RetailPrice,
		Discount,
		SalePrice,
		ShowOnLive,
		DisplayName,
		CatgoryId,
		Colors,
		ItemDetails,
		Stock,
		Images,
		IsTrending,
		PrimaryFileName,
		IsDeleted
    )VALUES(
        @Manufacture,
		@Brand,
		@ModelName,
		@SerialNos,
		@Warranty,
		@PurchaseDate,
		@PurchasePrice,
		@PQty,
		@RetailPrice,
		@Discount,
		@SalePrice,
		@ShowOnLive,
		@DisplayName,
		@CatgoryId,
		@Colors,
		@ItemDetails,
		@PQty,
		@Images,
		@IsTrending,
		@PrimaryFileName,
		0
    );
    END
    ELSE
    BEGIN
    UPDATE ItemMaster SET 
    Manufacture =@Manufacture,
		Brand =@Brand,
		ModelName = @ModelName,
		SerialNos = @SerialNos,
		Warranty = @Warranty,
		PurchaseDate = @PurchaseDate,
		PurchasePrice = @PurchasePrice,
		PQty =@PQty,
		RetailPrice = @RetailPrice,
		Discount =@Discount,
		SalePrice =@SalePrice,
		ShowOnLive =@ShowOnLive,
		DisplayName=@DisplayName,
		CatgoryId =@CatgoryId,
		ItemDetails = @ItemDetails,
		Colors =@Colors,
		Images =@Images,
		IsTrending =@IsTrending,
		PrimaryFileName =@PrimaryFileName
		WHERE 
		id = @id;
    END
    COMMIT TRANSACTION
END TRY
BEGIN CATCH
    DECLARE 
        @ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();
    RAISERROR (
        @ErrorMessage,
        @ErrorSeverity,
        @ErrorState    
        );
    ROLLBACK TRANSACTION
END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[getCategoryList]    Script Date: 10/19/2020 12:22:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Suraj Desai>
-- Create date: <22-09-2020>
-- Description:	<Get Catgory List>
-- =============================================
CREATE PROCEDURE [dbo].[getCategoryList] 
	@catName varchar(50)=Null
AS
BEGIN
	--SELECT CatId,CatName FROM CategoryMaster   WHERE CatName LIKE '%'+@catName+'%' AND IsDeleted=0;
	SELECT CatId,CatName,(SELECT COUNT(*) FROM ItemMaster WHERE CatgoryId= CategoryMaster.CatId)as 'catCount' FROM CategoryMaster   WHERE  IsDeleted=0;
END
GO
/****** Object:  StoredProcedure [dbo].[Delete_ItemMaster]    Script Date: 10/19/2020 12:22:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Suraj Desai>
-- Create date: <21-09-2020>
-- Description:	<To Insert Update ItemMaster>
-- =============================================
CREATE PROCEDURE [dbo].[Delete_ItemMaster] 
	(
	@id	int =0
	)
AS
BEGIN

BEGIN TRY
    BEGIN TRANSACTION
    
    UPDATE ItemMaster SET 
    IsDeleted = 1
		WHERE 
		id = @id;
    
    COMMIT TRANSACTION
END TRY
BEGIN CATCH
    DECLARE 
        @ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();
    RAISERROR (
        @ErrorMessage,
        @ErrorSeverity,
        @ErrorState    
        );
    ROLLBACK TRANSACTION
END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[AddUpdateStock]    Script Date: 10/19/2020 12:22:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Suraj Desai>
-- Create date: <21-09-2020>
-- Description:	<To Insert Update ItemMaster>
-- =============================================
CREATE PROCEDURE [dbo].[AddUpdateStock] 
	(
@id	int =0,
@Flag	varchar(50)=null,
@Stock numeric(18,2)=0.00,
@Podate	varchar(10)=null
	)
AS
DECLARE @oldStock numeric(18,2)
BEGIN

BEGIN TRY
    BEGIN TRANSACTION
    SET @oldStock = (SELECT Stock FROM ItemMaster WHERE id=@id);
    if(@Flag ='Add')
    BEGIN
    
    INSERT INTO PurchaseDetails
    (
    Podate,
    Qty,
    Itemid
    )
    VALUES(
    @Podate,
    @Stock,
    @id
    );
    UPDATE ItemMaster SET
    Stock = @oldStock+@Stock
    WHERE id =@id;
    
    END
    ELSE
    BEGIN
    UPDATE ItemMaster SET
    Stock = @oldStock-@Stock
    WHERE id =@id;
    END
    COMMIT TRANSACTION
END TRY
BEGIN CATCH
    DECLARE 
        @ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();
    RAISERROR (
        @ErrorMessage,
        @ErrorSeverity,
        @ErrorState    
        );
    ROLLBACK TRANSACTION
END CATCH
END
GO
