-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Suraj Desai>
-- Create date: <19-10-2020>
-- Description:	<TO insert Order>
-- =============================================
CREATE PROCEDURE Tran_Insert_Order 
	@OrderId	int =0,
@CustomerId	int=0,
@TotalPrice	numeric(18, 2)=0.00,
@DiscountAmount	numeric(18, 2)=0.00,
@NetAmount	numeric(18, 2)=0.00,
@UnderStatus	varchar(10)=0.00,
@OrderDate	varchar(10)=null,
@OrderUpdateDate	varchar(10)=null,
@stDetailXml varchar(MAX)=NULL	

AS

DECLARE @ItemId	int
DECLARE @Qauntity	int
DECLARE @Rate	decimal(18, 2)
DECLARE @DiscountAmount1	decimal(18, 2)
DECLARE @FinalPrice	decimal(18, 2)

DECLARE @TempDetailTable TABLE(
ItemId	int,
Qauntity	int,
Rate	decimal(18, 2),
DiscountAmount	decimal(18, 2),
FinalPrice	decimal(18, 2)
)

BEGIN
	BEGIN TRY
    BEGIN TRANSACTION
    
    INSERT INTO Order_Master (
CustomerId,
TotalPrice,
DiscountAmount,
NetAmount,
UnderStatus,
IsDeleted,
OrderDate,
OrderUpdateDate
)
VALUES(
@CustomerId,
@TotalPrice,
@DiscountAmount,
@NetAmount,
@UnderStatus,
0,
@OrderDate,
@OrderUpdateDate
)

SET @OrderId = SCOPE_IDENTITY();

--=========================== INsert Production Details ===========================
	DECLARE @intDocHandle int
		
		EXEC sp_xml_preparedocument @intDocHandle OUTPUT,@stDetailXml
		INSERT @TempDetailTable SELECT ItemId,Qauntity,Rate,DiscountAmount,FinalPrice FROM OPENXML(@intDocHandle,'/DocumentElement/dt',2)
		WITH
		(
			ItemId	int,
			Qauntity	int,
			Rate	decimal(18, 2),
			DiscountAmount	decimal(18, 2),
			FinalPrice	decimal(18, 2)
		)
		EXEC sp_xml_removedocument @intDocHandle
		
		DECLARE CurProduction CURSOR FOR SELECT ItemId,Qauntity,Rate,DiscountAmount,FinalPrice FROM @TempDetailTable
		OPEN CurProduction
			FETCH NEXT FROM CurProduction INTO @ItemId,@Qauntity,@Rate,@DiscountAmount1,@FinalPrice
			WHILE @@FETCH_STATUS=0
			BEGIN
				INSERT INTO OrderDetails
				(
					OrderId,
					ItemId,
					Qauntity,
					Rate,
					DiscountAmount,
					FinalPrice
				)
				VALUES
				(
					@OrderId,
					@ItemId,
					@Qauntity,
					@Rate,
					@DiscountAmount1,
					@FinalPrice
				)
			
			FETCH NEXT FROM CurProduction INTO  @ItemId,@Qauntity,@Rate,@DiscountAmount1,@FinalPrice
			END
			CLOSE CurProduction
			DEALLOCATE CurProduction
    
    COMMIT TRANSACTION
  END TRY
BEGIN CATCH
    DECLARE 
        @ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();
    RAISERROR (
        @ErrorMessage,
        @ErrorSeverity,
        @ErrorState    
        );
    ROLLBACK TRANSACTION
END CATCH
	
END
GO
