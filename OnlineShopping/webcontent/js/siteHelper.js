﻿
var url = "";

$(document).ready(function () {

    $.ajax({
        url: "DataProvider.asmx/GetCategoryNamesObj",
        method: "post",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ term: "" }),
        dataType: 'json',
        success: function (data) {
            console.log(data.d);
            var trendingCatStr = "";
            var allCatStr = "";
            var res = data.d;
            for (var i = 0; i < res.length; i++) {
                var element = res[i];
                allCatStr = allCatStr + "<a class=\"dropdown-item\" href=\"category.html?catId=" + element.CatId + "\">" + element.CatName + "</a>";
            }

            $("#catagoriesDiv").html(allCatStr);
        },
        error: function (err) {
            alert(err);
        }
    });

    $.ajax({
        url: "DataProvider.asmx/getTrendingProducts",
        method: "post",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ term: '' }),
        dataType: 'json',
        success: function (data) {
            console.log(data.d);
            var trendinItemStr = "";
            var res = data.d;
            for (var i = 0; i < res.length; i++) {
                var element = res[i];
                
                trendinItemStr = trendinItemStr + "<div class=\"col-sm-12 \">" +
                "<div class=\"product-item bg-light\">" +
                        "<div class=\"card\">" +
                                "<div class=\"thumb-content\"><a href=\"single.html?itemId=" + element.id + "\"> <img class=\"img-fluid\" src=\"Images/" + element.PrimaryFileName + "\" alt=\"Card image cap\"/> </a></div>" +
                                "<div class=\"card-body\"><h4 class=\"card-title\"><a href=\"single.html?itemId=" + element.id + "\">" + element.DisplayName + "</a></h4>" +
                                "<ul class=\"list-inline product-meta\">" +
                                    "<li class=\"list-inline-item\"><a href=\"single.html?itemId=" + element.id + "\"><i class=\"fa fa-folder-open-o\"></i>" + element.CategoryName + "</a></li>" +
                                        "<li class=\"list-inline-item\"><a href=\"#\"><i class=\"fa fa-calendar\"></i>26th December</a></li>" +
                "</ul>" +
                "<div class=\"price-info\">"+
                                        "<div class=\"d-flex mb-1\">"+
                                            "<div class=\"description\">Product MRP</div>"+
                                            "<div class=\"prod-amount lightgrey\">Rs."+ element.RetailPrice +"</div>"+
                                        "</div>"+
                                        "<div class=\"d-flex mb-1\">"+
                                            "<div class=\description\">Discount</div>"+
                                            "<div class=\"prod-amount lightgrey\">"+element.Discount+"%</div>"+
                                        "</div>"+
                                        "<div class=\"d-flex mb-1\">"+
                                            "<div class=\"description\">Final Price</div>"+
                                            "<div class=\"prod-amount\">Rs."+element.SalePrice+"</div>"+
                                        "</div>"+
                                    "</div>" +

                                    "<div class=\"product-ratings\">" +
                                    "<ul class=\"list-inline\">" +
                                        "<li class=\"list-inline-item selected\"><i class=\"fa fa-star\"></i></li>" +
                                            "<li class=\"list-inline-item selected\"><i class=\"fa fa-star\"></i></li>" +
                                            "<li class=\"list-inline-item selected\"><i class=\"fa fa-star\"></i></li>" +
                                            "<li class=\"list-inline-item selected\"><i class=\"fa fa-star\"></i></li>" +
                                            "<li class=\"list-inline-item\"><i class=\"fa fa-star\"></i></li>" +
                "</ul>" +
                "</div>" +
                "</div>" +
                "</div>" +
                            "</div>" +
                    "</div>"
            }
            $("#trendingSlide").html(trendinItemStr);
            $('.owl-carousel').owlCarousel({
                autoplay: true,
                lazyLoad: true,
                loop: true,
                margin: 10,
                nav: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 1
                    },
                    1000: {
                        items: 4
                    }
                }
            })
        },
        error: function (err) {
            alert(err);
        }
    });

    $("#txtSearchAnything").autocomplete({
        source: function (request, responce) {
            $.ajax({
                url: "DataProvider.asmx/getAllProductsBySearch",
                method: "post",
                contentType: "application/json;charset=utf-8",
                data: JSON.stringify({ term: request.term }),
                dataType: 'json',
                success: function (data) {
                    responce(data.d);
                },
                error: function (err) {
                    alert(err);
                }
            });
        },
        open: function (event, ui) {
            search_term = event.target.value;
            //var d = $(".ui-autocomplete").append(
            //  "<li class='ui-menu-item addcategory' data-toggle='modal' data-target='#AddCategory'>Add New Category </li>"
            //);
        }
    });
});