﻿$(document).ready(function () {

    var urlString = window.location.href;
    let paramString = urlString.split('?')[1];
    let queryString = new URLSearchParams(paramString);
    console.log(queryString);
    let params = [];
    for (let pair of queryString.entries()) {
        let obj = {};
        obj[pair[0]] = pair[1];
        params.push(obj);
    }
    let catid =parseInt(params[0]["catId"]);
    console.log("CategoeryId", catid);

    $.ajax({
        url: "DataProvider.asmx/GetCategoryNamesObj",
        method: "post",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ term: "" }),
        dataType: 'json',
        success: function (data) {
            console.log(data.d);
            var trendingCatStr = "";
            var allCatStr = "";
            var res = data.d;
            for (var i = 0; i < res.length; i++) {
                var element = res[i];
                if (element.CatId == catid.toString()) {
                    $("#singleCatName").text("Results For "+ element.CatName);
                    $("#catCount").text(element.catCount + " Results on " + new Date());
                }
                trendingCatStr = trendingCatStr + "<li><a href=\"category.html?catId=" + element.CatId + "\">" + element.CatName + " <span>" + element.catCount + "</span></a></li>";
                allCatStr = allCatStr + "<a class=\"dropdown-item\" href=\"category.html?catId=" + element.CatId + "\">" + element.CatName + "</a>";
            }

            $("#catagoriesDiv").html(allCatStr);
            $("#catListWithCount").html(trendingCatStr);
        },
        error: function (err) {
            alert(err);
        }
    });

    $.ajax({
        url: "DataProvider.asmx/GetProductsByCategoryId",
        method: "post",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ term: catid }),
        dataType: 'json',
        success: function (data) {
            console.log(data.d);
            var trendingItemStr = "";
            var res = data.d;
            for (var i = 0; i < res.length; i++) {
                var element = res[i];
                trendingItemStr = trendingItemStr + "<div class=\"col-sm-6 col-lg-4 col-md-6\">" +

                                "<div class=\"product-item bg-light\">" +
                                    "<div class=\"card\">" +
                                        "<div class=\"thumb-content\">" +

                                            "<a href=\"single.html?itemId=" + element.id + "\">" +
                                                "<img class=\"img-fluid\" src=\"Images/" + element.PrimaryFileName + "\" alt=\"Card image cap\">" +
                                            "</a>" +
                                        "</div>" +
                                        "<div class=\"card-body\">" +
                                            "<h4 class=\"card-title\"><a href=\"single.html?itemId=" + element .id+ "\">" + element.DisplayName + "</a>" +
                                             "</h4>" +
                                           " <ul class=\"list-inline product-meta\">" +
                                                "<li class=\"list-inline-item\">" +
                                                   " <a href=\"single.html\"><i class=\"fa fa-folder-open-o\"></i>" + element.CategoryName + "</a>" +
                                                "</li>" +
                                                "<li class=\"list-inline-item\">" +
                                                    "<a href=\"#\"><i class=\"fa fa-calendar\"></i>26th December</a>" +
                                                "</li>" +
                                            "</ul>" +
                                            "<p class=\"card-text\">" + element.ItemDetail + "</p>" +
                                            "<div class=\"product-ratings\">" +
                                                "<ul class=\"list-inline\">" +
                                                    "<li class=\"list-inline-item selected\"><i class=\"fa fa-star\"></i></li>" +
                                                    "<li class=\"list-inline-item selected\"><i class=\"fa fa-star\"></i></li>" +
                                                    "<li class=\"list-inline-item selected\"><i class=\"fa fa-star\"></i></li>" +
                                                    "<li class=\"list-inline-item selected\"><i class=\"fa fa-star\"></i></li>" +
                                                    "<li class=\"list-inline-item\"><i class=\"fa fa-star\"></i></li>" +
                                                "</ul>" +
                                            "</div>" +
                                        "</div>" +
                                    "</div>" +
                                "</div>" +
                          "</div>"
            }

            $("#allProducts").html(trendingItemStr);
        },
        error: function (err) {
            alert(err);
        }
    });

});