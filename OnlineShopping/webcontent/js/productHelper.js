﻿$(document).ready(function () {

    var urlString = window.location.href;
    let paramString = urlString.split('?')[1];
    let queryString = new URLSearchParams(paramString);
    console.log(queryString);
    let params = [];
    for (let pair of queryString.entries()) {
        let obj = {};
        obj[pair[0]] = pair[1];
        params.push(obj);
    }
    let itemId = parseInt(params[0]["itemId"]);

    $.ajax({
        url: "DataProvider.asmx/GetProductsById",
        method: "post",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ term: itemId }),
        dataType: 'json',
        success: function (data) {
            console.log(data.d);
            var trendingItemStr = "";
            var res = data.d;
            
            var res = data.d;
            var element = res[0];
            var ProductsInfo = "";
            $("#ProductDescription").text(element.DisplayName);
            $("#ItemDetailJSON").text(JSON.stringify(element));
            ProductsInfo = "<i class=\"fa fa-folder-open-o\"></i> Category <a href=\"\">" + element.CatName + "</a>"
            $("#ProductCategoryName").html(ProductsInfo);
            $("#SalePriceRange").text("Rs. " + element.RetailPrice);
            $("#DiscountPrice").text(element.Discount);
            $("#FinalPrice").text(element.SalePrice);
            $("#ProDetails").text(element.ItemDetail);
            var imgfiles = element.Images;
            var imgarray = imgfiles.split(',');
            var imghtmllist = "";
            var mainImg = "<img id=\"productimage\" src=\"Images/" + element.PrimaryFileName + "\" alt=\"product-img\"/>"
            $("#imgProductPreview").html(mainImg);
            for (var i = 0; i <imgarray.length; i++) {
                // imghtmllist = imghtmllist + "<div class=\"product-slider-item my-4\" data-image=\"Images/" + imgarray[i] + "\">" +
                //   "<img class=\"img-fluid w-100\" src=\"Images/" + imgarray[i] + "\" alt=\"product-img\"/>" +
                // "</div>";
                if (i <= 4) {
                    imghtmllist = imghtmllist + "<div class=\"thumbnail\"><img src=\"Images/" + imgarray[i] + "\" alt=\"product-img\" style=\"height: 100px;\"/></div>"
                }
               
            }
            $("#productImages").html(imghtmllist);
            $('.owl-carousel').owlCarousel({
                autoplay: true,
                lazyLoad: true,
                loop: true,
                margin: 10,
                nav: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 1
                    },
                    1000: {
                        items: 1
                    }
                }
            })
            $(".thumbnail img").click(function () {
                $("#productimage").attr('src', ($(this).attr('src')));
            });
                //$("#allProducts").html(trendingItemStr);
        },
        error: function (err) {
            alert(err);
        }
    });

});

function BuyNow() {
    let itemList = [];

    if (sessionStorage.getItem("itemList") == null || sessionStorage.getItem("itemList") == undefined)
    {
        itemList = JSON.parse(sessionStorage.getItem("itemList"));
        items = document.getElementById("ItemDetailJSON").innerText;
        itemList = [JSON.parse(items)];
        sessionStorage.setItem("itemList", JSON.stringify(itemList));
    }
        else
    {
        items = document.getElementById("ItemDetailJSON").innerText;
        itemList.push(JSON.parse(items));
        sessionStorage.setItem("itemList", JSON.stringify(itemList));
    }
    //window.location("CustmerOrder.html");
    let loginInfo = sessionStorage.getItem("loggedInUser");
    if (loginInfo == null || loginInfo == undefined || loginInfo == "") {
        sessionStorage.setItem("RequestedPage", "Order");
        window.location.href = "Login.html";
    } else {
        sessionStorage.setItem("lastAccesedPage", window.location.href);
        window.location.href = "CustomerOrder.html";
    }
   
}