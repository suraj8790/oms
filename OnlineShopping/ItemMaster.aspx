﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="ItemMaster.aspx.cs" Inherits="OnlineShopping.UI.ItemMaster" %>

<%@ MasterType VirtualPath="~/Master.Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <section class="admin"  id="AddNewProduct" style="display: block">
                <div class="container py-5">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="email">Show uploaded images:</label>
                                <div runat="server" id="divUploadedImages" class="image-upload-container" style="min-height:135px">
                                    <%--<img src="content/media/honor-30s.jpg" />
                                    <img src="content/media/honor-8s.jpg" />
                                    <img src="content/media/huawei-nova-7-pro-5g.jpg" />
                                    <img src="content/media/oppo-a92.jpg" />--%>
                                </div>
                                <asp:Label runat="server" ID="lblFileNames" Visible="false" />
                                <asp:FileUpload runat="server" ID="UploadImages" AllowMultiple="true" TabIndex="16" />
                                <asp:Button runat="server" ID="uploadedFile" Text="Upload" OnClick="uploadedFile_Click" TabIndex="17" />
                            </div>
                            <div class="form-group">
                                <label for="email">Show SingleUploaded images:</label>
                                <div runat="server" id="div1SingleUploadImages" class="image-upload-container" style="min-height:135px">

                                </div>
                                <asp:Label runat="server" ID="SinglelblFileNames" Visible="false" />
                                <asp:FileUpload runat="server" ID="SingleUploadImages" AllowMultiple="true" TabIndex="16" />
                                <asp:Button runat="server" ID="SingleuploadedFile" Text="Upload" OnClick="SingleButton1_Click" TabIndex="17" />
                            </div>
                            <div class="form-group">
                                <label for="comment">Comment:</label>
                                <asp:TextBox runat="server" class="form-control" TextMode="MultiLine" Rows="5" ID="txtItemDetail" MaxLength="500" TabIndex="18"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="txtProductCategory">Category:</label>
                                <%--<input
                            type="text"
                            class="form-control"
                            id="ProductCategory"
                            placeholder="Select Category" />--%>
                                <div class="row">
                                    <div class="col-md-10">
                                        <asp:DropDownList runat="server" ID="drpCategory" class="form-control" SelectedIndexChanged="drpCategory_SelectedIndexChanged" TabIndex="1">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-2" style="text-align: left">
                                        <asp:Button runat="server" ID="btnAddCat" data-toggle='modal' class="btn btn-primary" data-target='#AddCategory' Text="Add"></asp:Button>
                                    </div>
                                </div>
                                <%--<asp:textbox runat="server" class="form-control" placeholder="Select Category" ID="txtProductCategory"></asp:textbox>--%>
                            </div>
                            <div class="form-group">
                                <label for="txtManufactureName">Manufacturer:</label>
                                <asp:TextBox runat="server" class="form-control" placeholder="Manufacturer Name" ID="txtManufactureName" AutoPostBack="true" OnTextChanged="txtManufactureName_TextChanged" MaxLength="90" TabIndex="2"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="txtBrandName">Brand</label>
                                <asp:TextBox type="text" runat="server" class="form-control" placeholder="Brand Name" ID="txtBrandName" AutoPostBack="true" OnTextChanged="txtBrandName_TextChanged" MaxLength="45" TabIndex="3"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="txtModelName">Model Name</label>
                                <asp:TextBox runat="server" class="form-control" placeholder="Model Name" ID="txtModelName" AutoPostBack="true" OnTextChanged="txtModelName_TextChanged" MaxLength="45" TabIndex="4"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="txtSerialNo">Serial No. / IMEI</label>
                                <asp:TextBox runat="server" class="form-control" placeholder="Serial No." ID="txtSerialNo" MaxLength="190" TabIndex="5"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="txtWarranty">Warranty (eg.1 year or 6 months)</label>
                                <asp:TextBox runat="server" class="form-control" placeholder="Warranty" ID="txtWarranty" MaxLength="10" TabIndex="6"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="txtPurchaseDate">Purchase Date (eg. 01-01-2020)</label>
                                <asp:TextBox runat="server" class="form-control" placeholder="Select date" ID="txtPurchaseDate" MaxLength="10" TabIndex="7"></asp:TextBox>
                            </div>
                            <div class="form-check mt-5">
                                <asp:CheckBox runat="server" class="form-check-input" ID="chkIsTrending" Checked="true" />
                                <label class="form-check-label" for="chkIsTrending">
                                    Trending 
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="txtDisplayName">Dispaly Name</label>
                                <asp:TextBox runat="server" class="form-control" placeholder="Display Name" ID="txtDisplayName" MaxLength="150" TabIndex="8"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="txtPurchasePrice">Purchase price per unit</label>
                                <asp:TextBox runat="server" class="form-control" placeholder="Purchase Price" ID="txtPurchasePrice" onkeypress="return isNumberKey(event,this)" MaxLength="9" TabIndex="9"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="txtQuantity">Purchase Quantity</label>
                                <asp:TextBox runat="server" class="form-control" placeholder="Quantity" ID="txtQuantity" onkeypress="return onlyNumber(event)" MaxLength="9" TabIndex="10"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="txtPrice">Retail Price:</label>
                                <asp:TextBox runat="server" class="form-control" placeholder="Price" ID="txtPrice" AutoPostBack="true" OnTextChanged="txtPrice_TextChanged" onkeypress="return isNumberKey(event,this)" MaxLength="9" TabIndex="11"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="txtDiscount">Discount</label>
                                <asp:TextBox runat="server" class="form-control" placeholder="Discount" ID="txtDiscount" AutoPostBack="true" OnTextChanged="txtDiscount_TextChanged" onkeypress="return isNumberKey(event,this)" MaxLength="9" TabIndex="12"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="txtSalePrice">Sales Price</label>
                                <asp:TextBox runat="server" class="form-control" placeholder="Final Sales Price" AutoPostBack="true" ID="txtSalePrice" onkeypress="return isNumberKey(event,this)" OnTextChanged="txtSalePrice_TextChanged" MaxLength="9" TabIndex="13"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="txtColors">Colors</label>
                                <asp:TextBox runat="server" class="form-control" placeholder="Available Colors" ID="txtColors" MaxLength="100" TabIndex="14"></asp:TextBox>
                            </div>
                            <div class="form-check mt-5">
                                <asp:CheckBox runat="server" class="form-check-input" ID="chkShowOnLive" Checked="true" TabIndex="15" />
                                <label class="form-check-label" for="chkShowOnLive">
                                    Show on live website
                                </label>
                            </div>

                            <div class="row mt-5">
                                <div class="col-md-6 col-sm-6">

                                    <asp:Button runat="server" ID="btnSave" class="btn btn-primary btn-block" Text="Save" OnClick="btnSave_Click" TabIndex="19"></asp:Button>

                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <asp:Button runat="server" ID="btnCancel" class="btn btn-danger btn-block" Text="Cancel" OnClick="btnCancel_Click1"
                                         TabIndex="20"></asp:Button>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <asp:HiddenField ID="hdnID" runat="server" />
                            <asp:HiddenField ID="hdnMode" runat="server" />
                        </div>
                    </div>
                </div>
            </section>
            <section class="admin"  id="Inventory">

                <div class="container">
                    <div class="row pt-3">
                        <div class="col-md-3">
                            <div class="form-group">
                                <asp:TextBox runat="server" class="form-control form-control-sm" placeholder="Search by DisplayName" ID="txtSearchTerm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-3 text-right">
                            <div class="dropdown">
                                <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Filter By
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Company Name</a>
                                    <a class="dropdown-item" href="#">Price</a>
                                    <a class="dropdown-item" href="#">Quantity</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <%--OnPageIndexChanging="grdSearch_PageIndexChanging" OnRowDeleting="grdSearch_RowDeleting" OnRowEditing="grdSearch_RowEditing" OnRowCommand="grdSearch_RowCommand"--%>
                        <asp:GridView ID="grdSearch" runat="server" CssClass="table table-striped inventorytable" AutoGenerateColumns="false"
                            OnPageIndexChanging="grdSearch_PageIndexChanging" OnRowDeleting="grdSearch_RowDeleting" OnRowEditing="grdSearch_RowEditing" OnRowCommand="grdSearch_RowCommand" EmptyDataText="Records Not Found">
                            <Columns>
                                <asp:TemplateField HeaderText="Sr. No">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Display Name" DataField="DisplayName" ItemStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Retail Price" DataField="RetailPrice" ItemStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Discount" DataField="Discount" ItemStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Sale Price" DataField="SalePrice" ItemStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Available Stock" DataField="Stock" ItemStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:ImageButton ImageUrl="content/media/pencil.png" CssClass="iconEdit" runat="server" ID="LinkButtonEdit" CommandName="Edit" CommandArgument='<%# Eval("id") %>' OnClientClick="changeTab('liAddNewProduct')"
                                            Text="Edit" />
                                    </ItemTemplate>

                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:ImageButton ImageUrl="content/media/bin.png" CssClass="iconEdit" runat="server" ID="LinkButtonDelete" CommandName="Delete" CommandArgument='<%# Eval("id") %>'
                                            Text="" OnClientClick="changeTabDelete()" />
                                    </ItemTemplate>

                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Add Stock" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:ImageButton ImageUrl="content/media/Edit.png" CssClass="iconEdit" runat="server" data-toggle='modal' data-target='#AddStock' ID="LinkButtonAddStock" CommandName="AddStock" CommandArgument='<%# Eval("id") %>'
                                            Text="Edit" />
                                    </ItemTemplate>

                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>



                        </asp:GridView>
                    </div>
                </div>
            </section>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="uploadedFile" />
            <asp:PostBackTrigger ControlID="SingleuploadedFile" />
            <asp:PostBackTrigger ControlID="txtManufactureName" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
